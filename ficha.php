<?php
require_once 'php/app.php';
$fs_ficha = call_function((object) ['method' => 'fs_ficha', 'iddist' => $_GET['iddist']]);
?>
<table class="table table-sm table-mariconada" border="0">
    <tbody>
        <tr>
            <td colspan="4" class="text-center bold" style="background: #1976d2; color: #fff;">MUNICIPALIDAD DE <?php echo strtoupper($fs_ficha->municipalidad); ?></td>
        </tr>
        <tr>
            <td class="bold">Ubigeo</td>
            <td><?php echo $fs_ficha->ubigeo; ?></td>
            <td class="bold">Municipalidad</td>
            <td><?php echo $fs_ficha->municipalidad; ?></td>
        </tr>
        <tr>
            <td class="bold">Autoridad</td>
            <td colspan="3"><?php echo $fs_ficha->autoridad; ?></td>
        </tr>
        <tr>
            <td class="bold">Partido</td>
            <td colspan="3"><?php echo $fs_ficha->partido; ?></td>
        </tr>
        <tr>
            <td class="bold">Población</td>
            <td colspan="3"><?php echo number_format($fs_ficha->poblacion); ?></td>
        </tr>
        <tr>
            <td class="bold">Central de emergencia</td>
            <td><?php echo $fs_ficha->existe_central_de_emergencia; ?></td>
            <td class="bold">Central telefónica</td>
            <td><?php echo $fs_ficha->central_telefonica_si_no; ?></td>
        </tr>
        <tr>
            <td class="bold">Estado de central telefónica</td>
            <td><?php echo $fs_ficha->estado_de_la_central_telefonica; ?></td>
            <td class="bold">Operadores por turno</td>
            <td><?php echo $fs_ficha->nro_operadores_por_turno; ?></td>
        </tr>
        <tr>
            <td class="bold">Horas de atención</td>
            <td><?php echo $fs_ficha->nro_de_horas_de_atencion; ?></td>
            <td class="bold">Llamadas registradas</td>
            <td><?php echo $fs_ficha->nro_de_llamadas_registradas; ?></td>
        </tr>
        <tr>
            <td class="bold">Llamadas atendidas</td>
            <td><?php echo $fs_ficha->nro_de_llamadas_atendidas; ?></td>
            <td class="bold">Redes sociales</td>
            <td><?php echo $fs_ficha->uso_redes_sociales_si_no; ?></td>
        </tr>
        <tr>
            <td class="bold">Ambulancias</td>
            <td><?php echo $fs_ficha->nro_de_ambulancias; ?></td>
            <td class="bold">Unidades móviles operativas</td>
            <td><?php echo $fs_ficha->nro_de_unidades_moviles_operativas; ?></td>
        </tr>
        <tr>
            <td class="bold">Llamadas malintencionadas</td>
            <td><?php echo $fs_ficha->nro_de_llamadas_malintencionadas; ?></td>
            <td class="bold">Graban llamadas</td>
            <td><?php echo $fs_ficha->graban_las_llamadas_si_no; ?></td>
        </tr>
        <tr>
            <td class="bold">Radios</td>
            <td><?php echo $fs_ficha->nro_de_radios; ?></td>
            <td class="bold">Integración con 105</td>
            <td><?php echo $fs_ficha->integracion_con_105_pnp_si_no; ?></td>
        </tr>
        <tr>
            <td class="bold">Serenos</td>
            <td><?php echo $fs_ficha->nro_de_serenos; ?></td>
            <td class="bold">Drones</td>
            <td><?php echo $fs_ficha->nro_de_drones; ?></td>
        </tr>
        <tr>
            <td class="bold">Cámaras</td>
            <td><?php echo $fs_ficha->nro_de_camaras; ?></td>
            <td class="bold">Garantía</td>
            <td><?php echo $fs_ficha->tiempo_garantia_anos; ?></td>
        </tr>
        <tr>
            <td class="bold">Tiempo de almacenamiento de videos</td>
            <td><?php echo $fs_ficha->tiempo_de_almacenamiento_de_videos_dias; ?></td>
            <td class="bold">Monitores</td>
            <td><?php echo $fs_ficha->nro_monitores; ?></td>
        </tr>
        <tr>
            <td class="bold">Año de implementación</td>
            <td><?php echo $fs_ficha->fecha_de_implementacion; ?></td>
            <td class="bold">Nível de satisfacción</td>
            <td><?php echo $fs_ficha->nivel_de_satisfaccion; ?></td>
        </tr>
    </tbody>
</table>
