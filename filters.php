<?php
require_once 'php/app.php';
?>
<div id="sidebar" class="bcg-transparent">
    <div class="panel panel-default not-edge not-background">
        <div class="panel-heading text-center">
            <strong>
                <!--CRITERIOS DE BÚSQUEDA-->
                Cantidad de puntos de alta incidencia 2016-2019
            </strong>
        </div>
        <div class="panel-collapse collapse in" role="tabpanel">
            <div class="panel-body">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default not-edge">
                        <div class="panel-heading" role="tab">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#criteriosBusqueda">
                                    Búsqueda por ubigeo
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table id="tblDetUbigeo" class="table">
                                            <thead>
                                                <tr>
                                                    <td></td>
                                                    <td>Provincia</td>
                                                    <td>Muy alto</td>
                                                    <td>Alto</td>
                                                    <td>Medio</td>
                                                    <td>Bajo</td>
                                                    <td>Muy bajo</td>
                                                    <td>Total</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $fs_hexagonos_prov = call_function((object) ['method' => 'fs_hexagonos_prov']);?>
                                                <?php foreach ($fs_hexagonos_prov as $key_prov) {?>
                                                    <tr>
                                                        <td><input type="button" class="btn btn-xs btn-toogle" value="+" data-ubigeo="<?php echo $key_prov->codubigeo; ?>" data-nivel="1"/></td>
                                                        <td><a href="#" data-cql="idprov='<?php echo $key_prov->codubigeo; ?>'"><?php echo $key_prov->ubigeo; ?></a></td>
                                                        <td class="text-right"><a href="#" data-cql="idprov='<?php echo $key_prov->codubigeo; ?>' and clasifica = 5"><?php echo $key_prov->muy_alto; ?></a></td>
                                                        <td class="text-right"><a href="#" data-cql="idprov='<?php echo $key_prov->codubigeo; ?>' and clasifica = 4"><?php echo $key_prov->alto; ?></a></td>
                                                        <td class="text-right"><a href="#" data-cql="idprov='<?php echo $key_prov->codubigeo; ?>' and clasifica = 3"><?php echo $key_prov->medio; ?></a></td>
                                                        <td class="text-right"><a href="#" data-cql="idprov='<?php echo $key_prov->codubigeo; ?>' and clasifica = 2"><?php echo $key_prov->bajo; ?></a></td>
                                                        <td class="text-right"><a href="#" data-cql="idprov='<?php echo $key_prov->codubigeo; ?>' and clasifica = 1"><?php echo $key_prov->muy_bajo; ?></a></td>
                                                        <td class="text-right"><a href="#" data-cql="idprov='<?php echo $key_prov->codubigeo; ?>'"><?php echo $key_prov->total; ?></a></td>
                                                    </tr>
                                                    <tr class="hide">
                                                        <td colspan="8">
                                                            <table id="tbl_<?php echo $key_prov->codubigeo; ?>" class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td>Distrito</td>
                                                                        <td>Muy alto</td>
                                                                        <td>Alto</td>
                                                                        <td>Medio</td>
                                                                        <td>Bajo</td>
                                                                        <td>Muy bajo</td>
                                                                        <td>Total</td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody></tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                <?php }?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row hide">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Ubigeo</label>
                                            <select id="ddlUbigeo" class="form-control form-control-sm" style="height: 500px;" multiple>
                                                <optgroup label="CALLAO">
                                                    <?php $fs_dist_lima = call_function((object) ['method' => 'fs_dist', 'codprov' => '0701']);?>
                                                    <?php foreach ($fs_dist_lima as $key) {?>
                                                        <option value="<?php echo $key->iddist; ?>"><?php echo $key->distrito; ?></option>
                                                    <?php }?>
                                                </optgroup>
                                                <optgroup label="LIMA">
                                                    <?php $fs_dist_lima = call_function((object) ['method' => 'fs_dist', 'codprov' => '1501']);?>
                                                    <?php foreach ($fs_dist_lima as $key) {?>
                                                        <option value="<?php echo $key->iddist; ?>"><?php echo $key->distrito; ?></option>
                                                    <?php }?>
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row hide">
                                    <div class="col-lg-12 text-center">
                                        <div class="form-group">
                                            <button type="button" id="btnFiltrar" class="btn btn-primary btn-sm" onclick="actualizar_mapa()">
                                                <i class="fa fa-check"></i>
                                                Actualizar mapa
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<!-- -->
		<div class="panel-collapse collapse in" role="tabpanel">
            <div class="panel-body">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default not-edge">
                        <div class="panel-heading" role="tab">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#criteriosBusqueda">
                                    Búsqueda por vías
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table id="tblDetVia" class="table">
                                            <thead>
                                                <tr>
                                                    <td></td>
                                                    <td>Vía</td>
                                                    <td>Muy alto</td>
                                                    <td>Alto</td>
                                                    <td>Medio</td>
                                                    <td>Bajo</td>
                                                    <td>Muy bajo</td>
                                                    <td>Total</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $fs_vias = call_function((object) ['method' => 'fs_vias']);?>
                                                <?php foreach ($fs_vias as $key_via) {?>
                                                    <tr>
                                                        <td><input type="button" class="btn btn-xs btn-toogle" value="+" data-codvia="<?php echo $key_via->cod_via; ?>"/></td>
                                                        <td><a href="#" data-cql="<?php echo $key_via->cod_via; ?> > 0"><?php echo $key_via->via; ?></a></td>
                                                        <td class="text-right"><a href="#" data-cql="clasifica = 5 and <?php echo $key_via->cod_via; ?> > 0"><?php echo $key_via->muy_alto; ?></a></td>
                                                        <td class="text-right"><a href="#" data-cql="clasifica = 4 and <?php echo $key_via->cod_via; ?> > 0"><?php echo $key_via->alto; ?></a></td>
                                                        <td class="text-right"><a href="#" data-cql="clasifica = 3 and <?php echo $key_via->cod_via; ?> > 0"><?php echo $key_via->medio; ?></a></td>
                                                        <td class="text-right"><a href="#" data-cql="clasifica = 2 and <?php echo $key_via->cod_via; ?> > 0"><?php echo $key_via->bajo; ?></a></td>
                                                        <td class="text-right"><a href="#" data-cql="clasifica = 1 and <?php echo $key_via->cod_via; ?> > 0"><?php echo $key_via->muy_bajo; ?></a></td>
                                                        <td class="text-right"><a href="#" data-cql="<?php echo $key_via->cod_via; ?> > 1"><?php echo $key_via->total; ?></a></td>
                                                    </tr>
                                                    <tr class="hide">
                                                        <td colspan="8">
                                                            <table id="tbl_<?php echo $key_via->cod_via; ?>" class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <td>Distrito</td>
                                                                        <td>Muy alto</td>
                                                                        <td>Alto</td>
                                                                        <td>Medio</td>
                                                                        <td>Bajo</td>
                                                                        <td>Muy bajo</td>
                                                                        <td>Total</td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody></tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                <?php }?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
