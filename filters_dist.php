<?php
require_once 'php/app.php';
$data = json_decode($_GET['data']);
$fs_hexagonos_dist = call_function((object) ['method' => 'fs_hexagonos_dist', 'codubigeo' => $data->ubigeo]);

foreach ($fs_hexagonos_dist as $key_dist) {
    ?>
    <tr>
        <td><input type="button" class="btn btn-xs btn-toogle" value="+" data-ubigeo="<?php echo $key_dist->codubigeo; ?>" data-nivel="2"/></td>
        <td><a href="#" data-cql="iddist='<?php echo $key_dist->codubigeo; ?>'"><?php echo $key_dist->ubigeo; ?></a></td>
        <td class="text-right"><a href="#" data-cql="iddist='<?php echo $key_dist->codubigeo; ?>' and clasifica = 5"><?php echo $key_dist->muy_alto; ?></a></td>
        <td class="text-right"><a href="#" data-cql="iddist='<?php echo $key_dist->codubigeo; ?>' and clasifica = 4"><?php echo $key_dist->alto; ?></a></td>
        <td class="text-right"><a href="#" data-cql="iddist='<?php echo $key_dist->codubigeo; ?>' and clasifica = 3"><?php echo $key_dist->medio; ?></a></td>
        <td class="text-right"><a href="#" data-cql="iddist='<?php echo $key_dist->codubigeo; ?>' and clasifica = 2"><?php echo $key_dist->bajo; ?></a></td>
        <td class="text-right"><a href="#" data-cql="iddist='<?php echo $key_dist->codubigeo; ?>' and clasifica = 1"><?php echo $key_dist->muy_bajo; ?></a></td>
        <td class="text-right"><a href="#" data-cql="iddist='<?php echo $key_dist->codubigeo; ?>'"><?php echo $key_dist->total; ?></a></td>
    </tr>
    <tr class="hide">
        <td colspan="8">
            <table id="tbl_<?php echo $key_dist->codubigeo; ?>" class="table">
                <thead>
                    <tr>
                        <td>Vía</td>
                        <td>Muy alto</td>
                        <td>Alto</td>
                        <td>Medio</td>
                        <td>Bajo</td>
                        <td>Muy bajo</td>
                        <td>Total</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </td>
    </tr>
<?php }?>