<!-- Desarrollado por :  
Correo de contacto:
Cel de contacto:
-->

<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// if (!isset($_SESSION['user'])) {
//     header('Location: login.php');
// }
?>
<html>

<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="http://siae.colaboraccion.pe/assets/leaflet/leaflet.css" />
    <link rel="stylesheet" href="http://siae.colaboraccion.pe/assets/extramarkers/css/leaflet.extra-markers.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://siae.colaboraccion.pe/assets/geocoder/esri-leaflet-geocoder.css" />
    <link rel="stylesheet" href="http://siae.colaboraccion.pe/assets/sidebar/L.Control.Sidebar.css" />
    <link rel="stylesheet" href="http://siae.colaboraccion.pe/assets/panelayers/css/leaflet-panel-layers.css" />
    <link rel="stylesheet" href="https://unpkg.com/@geoman-io/leaflet-geoman-free@latest/dist/leaflet-geoman.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">

    <style>
        body {
            padding: 0;
            margin: 0;
        }

        html,
        body,
        #map {
            height: 100%;
        }

        button.easy-button-button {
            height: 30px;
            width: 30px;
            color: white;
            background: #053e74;
            border: 0px solid #053e74;
        }

        #accordion .panel-heading {
            padding: 0;
        }

        #accordion .panel-title>a {
            display: block;
            padding: 0.5em 0.6em;
            outline: none;
            font-weight: normal;
            font-size: 14px;
            text-decoration: none;
        }

        #accordion .panel-title>a.accordion-toggle::before,
        #accordion a[data-toggle="collapse"]::before {
            content: "\e113";
            float: left;
            font-family: 'Glyphicons Halflings';
            margin-right: 1em;
        }

        #accordion .panel-title>a.accordion-toggle.collapsed::before,
        #accordion a.collapsed[data-toggle="collapse"]::before {
            content: "\e114";
        }

        .leaflet-sidebar>.leaflet-control {
            padding: 0px 0px;
        }

        .leaflet-sidebar .close {
            font-size: 18pt;
            background: transparent;
        }

        .not-background {
            background: none;
        }

        .not-edge {
            border: none;
            box-shadow: none;
        }

        .panel-default>.panel-heading {
            color: #fff;
            background-color: #053e74;
            border-color: #053e74;
        }

        .leaflet-container a[data-toggle="collapse"] {
            color: #fff;
        }

        button.easy-button-button {
            height: 30px;
            width: 30px;
            color: white;
            background: #053e74;
            border: 0px solid #053e74;
        }

        .bcg-transparent {
            background-color: rgba(77, 77, 77, .6) !important;
        }

        .close {
            opacity: 1;
            right: 10px !important;
            top: 8px !important;
            color: #fff !important;
        }

        .info {
            padding: 6px 8px;
            font: 14px/16px Arial, Helvetica, sans-serif;
            background: white;
            background: rgba(255, 255, 255, 0.8);
            box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);
            border-radius: 5px;
        }

        .info h4 {
            margin: 0 0 5px;
            color: #777;
        }

        .legend {
            text-align: left;
            line-height: 18px;
            color: #555;
        }

        .legend i {
            width: 18px;
            height: 18px;
            float: left;
            margin-right: 8px;
            opacity: 0.7;
        }

        table {
            font-size: 11px !important;
        }

        .table>thead>tr>td {
            font-weight: bold;
            vertical-align: middle;
            text-align: center;
        }

        .table>tbody>tr>td {
            padding: 2px;
            border-top: 0px solid #fff !important;
            border-bottom: 1px solid #053e7421 !important;
            vertical-align: middle;
        }

        .vias-nacionales {
            color: #960303;
        }

        .vias-metropolitanas {
            color: #ff1010;
        }

        .vias-arteriales {
            color: #0dff0d;
        }

        .vias-colectoras {
            color: #ff6600;
        }

        .vias-distritales {
            color: #5e83b8;
        }

        .ubigeos {
            color: #5b9df4;
        }

        .genericos {
            color: #45a012;
        }

        .delito {
            color: #fd0012;
        }


        .faltas {
            color: #fffe30;
        }

        .modal-lg {
            /*width: 1200px !important;*/
        }

        input[type=radio] {
            margin: 2px 0 0;
        }

        .leaflet-panel-layers-overlays .leaflet-panel-layers-group:last-child {
            display: none !important;
        }

        #map>div.leaflet-control-container>div.leaflet-top.leaflet-right>div>form>div.leaflet-panel-layers-base>div>label {
            /*display: none !important;*/
        }

        .nav-tabs {
            border-bottom: 0px solid #ddd;
        }

        .bold {
            font-weight: bold;
        }

        .table-mariconada tr td.bold {
            background: #ddebf8;
        }

        .table-total td.text-center {
            background: #ddebf8;
        }

        /** */
        .LegendHexNoPreventivos {
            padding: 6px 8px;
            background: white;
            background: rgba(255, 255, 255, 0.4);
            /*box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);*/
            /*border-radius: 5px;*/
            line-height: 24px;
            color: #555;

        }

        .LegendHexNoPreventivos h4 {
            text-align: center;
            font-size: 12px;
            margin: 2px 12px 8px;
            color: #777;
        }

        .LegendHexNoPreventivos span {
            position: relative;
            bottom: 3px;
        }

        .LegendHexNoPreventivos i {
            width: 18px;
            height: 18px;
            float: left;
            margin: 0 8px 0 0;
            opacity: 0.7;
        }

        /** */
        .LegendHexPreventivos {
            padding: 6px 8px;
            background: white;
            background: rgba(255, 255, 255, 0.4);
            /*box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);*/
            /*border-radius: 5px;*/
            line-height: 24px;
            color: #555;

        }

        .LegendHexPreventivos h4 {
            text-align: center;
            font-size: 12px;
            margin: 2px 12px 8px;
            color: #777;
        }

        .LegendHexPreventivos span {
            position: relative;
            bottom: 3px;
        }

        .LegendHexPreventivos i {
            width: 18px;
            height: 18px;
            float: left;
            margin: 0 8px 0 0;
            opacity: 0.7;
        }

        /** */
        .LegendPointNoPreventivos {
            padding: 6px 8px;
            background: white;
            background: rgba(255, 255, 255, 0.4);
            /*box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);*/
            /*border-radius: 5px;*/
            line-height: 24px;
            color: #555;

        }

        .LegendPointNoPreventivos h4 {
            text-align: center;
            font-size: 12px;
            margin: 2px 12px 8px;
            color: #777;
        }

        .LegendPointNoPreventivos span {
            position: relative;
            bottom: 3px;
        }

        .LegendPointNoPreventivos i {
            width: 18px;
            height: 18px;
            float: left;
            margin: 0 8px 0 0;
            opacity: 0.7;
        }


        .LegendPointPreventivos {
            padding: 6px 8px;
            background: white;
            background: rgba(255, 255, 255, 0.4);
            /*box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);*/
            /*border-radius: 5px;*/
            line-height: 24px;
            color: #555;

        }

        .LegendPointPreventivos h4 {
            text-align: center;
            font-size: 12px;
            margin: 2px 12px 8px;
            color: #777;
        }

        .LegendPointPreventivos span {
            position: relative;
            bottom: 3px;
        }

        .LegendPointPreventivos i {
            width: 18px;
            height: 18px;
            float: left;
            margin: 0 8px 0 0;
            opacity: 0.7;
        }

        .legendGroupLayers {
            display: none;
        }

        .legendGroupLayers:hover {
            cursor: move;
        }
    </style>
</head>

<body>
    <div id="map"></div>

    <?php include_once 'filters.php'; ?>
    <?php include_once 'charts.php'; ?>
    <?php include_once 'modal.php'; ?>

    <script type="text/javascript" src="http://siae.colaboraccion.pe/assets/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="http://siae.colaboraccion.pe/assets/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="http://siae.colaboraccion.pe/assets/leaflet/leaflet.js"></script>
    <script type="text/javascript" src="http://siae.colaboraccion.pe/assets/topojson/topojson.min.js"></script>
    <script type="text/javascript" src="http://siae.colaboraccion.pe/assets/simpleheat/leaflet-heat.js"></script>
    <script type="text/javascript" src="http://siae.colaboraccion.pe/assets/geocoder/esri-leaflet.js"></script>
    <script type="text/javascript" src="http://siae.colaboraccion.pe/assets/geocoder/esri-leaflet-geocoder.js"></script>
    <script type="text/javascript" src="http://siae.colaboraccion.pe/assets/easybutton/easy-button.js"></script>
    <script type="text/javascript" src="http://siae.colaboraccion.pe/assets/sidebar/L.Control.Sidebar.js"></script>
    <script type="text/javascript" src="http://siae.colaboraccion.pe/assets/extramarkers/js/leaflet.extra-markers.min.js"></script>
    <script type="text/javascript" src="http://siae.colaboraccion.pe/assets/panelayers/js/leaflet-panel-layers.js"></script>
    <script type="text/javascript" src="https://code.highcharts.com/highcharts.js"></script>
    <script type="text/javascript" src="https://code.highcharts.com/modules/exporting.js"></script>
    <script type="text/javascript" src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://unpkg.com/@geoman-io/leaflet-geoman-free@latest/dist/leaflet-geoman.min.js"></script>

    <script>
        var width_map, height_map;
        var flagCircle = 1;
        var map, cql_and = [],
            cql_or = [],
            lyr_mapa_calor = new L.layerGroup(),
            lyr_mapa_calor_idx = [],
            lyr_hex_idx = 0,
            cql_puntos = [];
        densidad = ['151', '152', '153', '154', '155', '156', '157', '158', '159', '160', '161'];
        var initDialog = function(width, height) {
            if (typeof dialog != 'undefined') {
                dialog.destroy();
            }

            dialog = L.control.dialog({
                anchor: [0, 0],
                size: [width, height],
                maxSize: [550, 550],
                initOpen: false
            }).addTo(map);
        }
        init();
        map();
        logo();
        search();
        legend();
        sidebar();
        buttons();
        panel_layers();
        lupa();
        $(document).ready(function() {



            console.log("ready");
            width_map = $('#map').width();
            height_map = $('#map').height();
            // var parent = overLayers.filter(p => p.key == 'densidad_punto').shift().layers;
            // console.log(parent);



            $("optgroup").on("click", function(e) {
                if (e.target.tagName === 'OPTGROUP') {
                    var status = $(this).children("option").length == $(this).children("option:selected").length;
                    $(this).children("option").prop("selected", !status);
                }
            });

            $(".btn-toogle").on("click", function(e) {
                /*var item = $(this);

                item.closest('tr').next('tr').toggleClass("hide");*/
            });

            //$("#tblDetUbigeo, #tblDetVia").on("click", "a", function(){
            //$('body').on('click', '#tblDetUbigeo a, #tblDetVia a', function() {
            //$("#tblDetUbigeo a, #tblDetVia a").on("click", function(e) {
            $("td").on("click", "a[data-cql]", function() {
                var cql = $(this).attr('data-cql');

                filtrar_imagen(cql);

                if (this.hasAttribute("data-ubigeo")) {
                    redraw_layer('ubigeos', $(this).attr('data-ubigeo'));
                } else {
                    redraw_layer('ubigeos', cql.toUpperCase());
                }
            });

            //$("#tblDetUbigeo").on("click", "input.btn-toogle", function(){
            //$('body').on('click', '#tblDetUbigeo input.btn-toogle', function() {
            //$("#tblDetUbigeo .btn-toogle").on("click", function(e) {
            $("td").on("click", "input[type='button']", function() {
                var item = $(this);
                var ubigeo = item.attr('data-ubigeo');
                var nivel = item.attr('data-nivel');
                var path = '';
                var tbl = '';

                switch (nivel) {
                    case '1':
                        path = 'filters_dist.php';
                        tbl = 'tbl_' + ubigeo
                        break;
                    case '2':
                        path = 'filters_vias.php';
                        tbl = 'tbl_' + ubigeo
                        break;
                }

                $.get(path + '?data=' + encodeURIComponent(JSON.stringify({
                    'ubigeo': ubigeo,
                    'nivel': nivel
                })), function(content) {
                    $('#' + tbl + ' tbody').html(content);
                    item.closest('tr').next('tr').toggleClass("hide");
                });
            });

            //$("#tblDetVia").on("click", "input.btn-toogle", function(){
            //$('body').on('click', '#tblDetVia input.btn-toogle', function() {
            $("#tblDetVia .btn-toogle").on("click", function(e) {
                var item = $(this);
                var codvia = item.attr('data-codvia');

                $.get('filters_vias_dist.php?data=' + encodeURIComponent(JSON.stringify({
                    'codvia': codvia
                })), function(content) {
                    $('#tbl_' + codvia + ' tbody').html(content);
                    item.closest('tr').next('tr').toggleClass("hide");
                });
            });

            $("#map").on("change", '.leaflet-panel-layers-group input[type="radio"]', function() {
                /*var layerid = $('.leaflet-panel-layers-group input[type="radio"]:checked');

                if($('#chkHexagonos').length == 0){
                    var container = $('.leaflet-panel-layers-base .leaflet-panel-layers-grouplabel .leaflet-panel-layers-title');
                    $('<input />', { type: 'checkbox', id: 'chkHexagonos', value: 'Prender/Apagar', checked: 'checked' }).insertBefore(container);
                }

                if(lyr_hex_idx == 0){
                    lyr_hex_idx++;
                    return;
                }

                if(lyr_mapa_calor_idx.length > 0){
                    var idx = lyr_mapa_calor_idx;
                    lyr_mapa_calor_idx = [];

                    lyr_mapa_calor.clearLayers();

                    for (i = 0; i < idx.length; i++) {
                        lyr_mapa_calor_idx.push(idx[i]);
                        mapa_calor_distrito(idx[i], layerid.val(), null);
                    }
                }*/
            });
            // $("#map").on("change", '.leaflet-panel-layers-overlays .leaflet-panel-layers-item', function() {
            //     var layerid = $('.leaflet-panel-layers-overlays .leaflet-panel-layers-item input[type="checkbox"]:checked');
            //     console.log(layerid.val());
            //     if (baselayer = overLayers.filter(p => p.key == 'densidad_punto').shift().id == layerid.val()) {

            //         var str = '<input type="time" id="hora" name="hora" min="00:00:00" max="23:00:00" required="">';
            //         $(str).insertAfter($('.leaflet-panel-layers-overlays .leaflet-panel-layers-item input[type="checkbox"]:checked'));
            //     } else if (!$(this).is(':checked')) {
            //         $('#hora').remove();
            //     }


            //     //     var over =  overLayers.filter

            // });
            $("#map").on("click", "#chkHexagonos", () => {
                var layerid = $('.leaflet-panel-layers-group input[type="radio"]:checked');


                if (layerid.length > 0) {
                    var parent = baseLayers.filter(p => p.key == 'ghexagonos').shift().layers.filter(p => p.id == layerid.val()).shift().parent;
                    var baselayer = overLayers.filter(p => p.group == 'mask_hexagonos').shift().layers.filter(p => p.key == parent).shift().layer;

                    if (!$(this).is(':checked') && $('#chkHexagonos').is(':checked')) {
                        console.log("deschequiado");
                        map.addLayer(baselayer);
                        legend();
                        $('#map').css('cursor', 'pointer');
                    } else {
                        map.removeLayer(baselayer);
                        removeLegendFromMap('info');
                        if (document.querySelector(`.LegendHexPreventivos`) != null) {
                            removeLegendFromMap('LegendHexPreventivos');
                        }
                        if (document.querySelector(`.LegendHexNoPreventivos`) != null) {
                            removeLegendFromMap('LegendHexNoPreventivos');
                        }
                        if (document.querySelector(`.LegendPointNoPreventivos`) != null) {
                            removeLegendFromMap('LegendPointNoPreventivos');
                        }
                        if (document.querySelector(`.LegendPointPreventivos`) != null) {
                            removeLegendFromMap('LegendPointPreventivos');
                        }

                        $('#map').css('cursor', 'grab');
                    }
                }
            });

            $('body').on('shown.bs.modal', '#myModal', function() {
                fs_grafico({
                    method: 'fs_hexagonos_prov_graf',
                    grafico: 'container1'
                });
                fs_grafico({
                    method: 'fs_hexagonos_vias_graf',
                    grafico: 'container2'
                });
                fs_grafico({
                    method: 'fs_hexagonos_operador_graf',
                    grafico: 'container3'
                });
            });
        });

        function init() {
            document.addEventListener('click', function(e) {
                if (e.target && (e.target.hasAttribute("data-id") || e.target.hasAttribute("id"))) {
                    const dataId = e.target.hasAttribute("id") ? e.target.getAttribute("id") : e.target.getAttribute("id");
                    console.log("evento", dataId);

                    switch (dataId) {
                        case "camara":
                            $.get({
                                url: 'camara.php',
                                success: function(response) {
                                    document.getElementById("content_dashboard").innerHTML = response
                                }
                            });
                            break;
                        case "dvehiculo":
                            var params = {
                                method: 'fs_analisis_det_densidad',
                                lanlng: {
                                    "lat": -12.013464927093839,
                                    "lng": -77.09569931030273
                                },
                                table: 'data_lima_x_punto',
                                radio: 1250,
                                tablehex: 'hex_lima_punto'
                            };
                            var hmtl = '';
                            var hmtlvelo = '';
                            var hmtl_estado = '';
                            var totalvelod = 0;
                            var totalvelol = 0;
                            var totalvelom = 0;
                            var totalvelox = 0;
                            var totalveloj = 0;
                            var totalvelov = 0;
                            var totalvelos = 0;

                            var totald = 0;
                            var totall = 0;
                            var totalm = 0;
                            var totalx = 0;
                            var totalj = 0;
                            var totalv = 0;
                            var totals = 0;
                            $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params)), function(response) {
                                if (response) {
                                    $.each(response, function(i, item) {
                                        totald = totald + (item.d == null ? 0 : parseInt(item.d));
                                        totall = totall + (item.l == null ? 0 : parseInt(item.l));
                                        totalm = totalm + (item.m == null ? 0 : parseInt(item.m));
                                        totalx = totalx + (item.x == null ? 0 : parseInt(item.x));
                                        totalj = totalj + (item.j == null ? 0 : parseInt(item.j));
                                        totalv = totalv + (item.v == null ? 0 : parseInt(item.v));
                                        totals = totals + (item.s == null ? 0 : parseInt(item.s));

                                        hmtl += '<tr>';
                                        hmtl += '<td>' + item.horat + '</td>';
                                        hmtl += '<td class="text-center">' + (item.d == null ? '0' : parseInt(item.d / 4)) + '</td>';
                                        hmtl += '<td class="text-center">' + (item.l == null ? '0' : parseInt(item.l / 4)) + '</td>';
                                        hmtl += '<td class="text-center">' + (item.m == null ? '0' : parseInt(item.m / 4)) + '</td>';
                                        hmtl += '<td class="text-center">' + (item.x == null ? '0' : parseInt(item.x / 4)) + '</td>';
                                        hmtl += '<td class="text-center">' + (item.j == null ? '0' : parseInt(item.j / 4)) + '</td>';
                                        hmtl += '<td class="text-center">' + (item.v == null ? '0' : parseInt(item.v / 4)) + '</td>';
                                        hmtl += '<td class="text-center">' + (item.s == null ? '0' : parseInt(item.s / 4)) + '</td>';
                                        hmtl += '</tr>';
                                    });
                                    hmtl += '<tr>';
                                    hmtl += '<td class="text-center">Total Promedio</td>';
                                    hmtl += '<td class="text-center">' + Math.round(parseInt(totald / 4) / 24) + '</td>';
                                    hmtl += '<td class="text-center">' + Math.round(parseInt(totall / 4) / 24) + '</td>';
                                    hmtl += '<td class="text-center">' + Math.round(parseInt(totalm / 4) / 24) + '</td>';
                                    hmtl += '<td class="text-center">' + Math.round(parseInt(totalx / 4) / 24) + '</td>';
                                    hmtl += '<td class="text-center">' + Math.round(parseInt(totalj / 4) / 24) + '</td>';
                                    hmtl += '<td class="text-center">' + Math.round(parseInt(totalv / 4) / 24) + '</td>';
                                    hmtl += '<td class="text-center">' + Math.round(parseInt(totals / 4) / 24) + '</td>';
                                    hmtl += '</tr>';
                                    // var popup = L.popup({
                                    //     closeButton: true,
                                    //     minWidth: 900
                                    // }).setLatLng(latlng).setContent(
                                    //     '<div class="row"><div class="col-lg-6"><div class="text-center"><h5><strong>Data Densidad vehicular desde el 01/12/2018 al 07/12/2018 </strong></h5></div><br><br><table border="0" class="table table-sm table-mariconada"><tr><td class="text-center bold">Hora</td><td class="text-center bold">Dom</td><td class="text-center bold">Lun</td><td class="text-center bold">Mar</td><td class="text-center bold">Mie</td><td class="text-center bold">Jue</td><td class="text-center bold">Vie</td><td class="text-center bold">Sab</td></tr>' + hmtl + '</table></div> <div class="col-lg-6"><div class="text-center"><h5><strong>Data Promedio de velocidades >10 km/h y < 100 km/h desde el 01/12/2018 al 07/12/2018 </strong></h5></div><br><table border="0" class="table table-sm table-mariconada"><tr><td class="text-center bold">Hora</td><td class="text-center bold">Dom</td><td class="text-center bold">Lun</td><td class="text-center bold">Mar</td><td class="text-center bold">Mie</td><td class="text-center bold">Jue</td><td class="text-center bold">Vie</td><td class="text-center bold">Sab</td></tr>' + hmtlvelo + '</table></div></div> <div><table border="0" class="table table-sm table-mariconada"><tr><td class="text-center bold">GEOCERCAS</td><td class="text-center bold">CANTIDAD</td></tr>' + hmtl_estado + '</table></div> <div> <strong>*La consulta se realizó con la coordenada central:</strong> (' + lat.toString().slice(0, 10) + '; ' + lng.toString().slice(0, 10) + ')<strong>y un radio de: </strong>' + Math.round(radio) + ' metros </div>'
                                    // ).openOn(map);

                                    document.getElementById("content_dashboard").innerHTML = '<div class="row"><div class="col-lg-12"><div class="text-center"><h5><strong>Data Densidad vehicular desde el 26/09/2022 al 02/10/2022 </strong></h5></div><br><br><table border="0" class="table table-sm table-mariconada"><tr><td class="text-center bold">Hora</td><td class="text-center bold">Dom</td><td class="text-center bold">Lun</td><td class="text-center bold">Mar</td><td class="text-center bold">Mie</td><td class="text-center bold">Jue</td><td class="text-center bold">Vie</td><td class="text-center bold">Sab</td></tr>' + hmtl + '</table></div> </div> '
                                }
                            });
                            console.log(html)
                            break;
                        case "pvelocidad":
                            var params_velocidad = {
                                method: 'fs_analisis_velo',
                                lanlng: {
                                    "lat": -12.013464927093839,
                                    "lng": -77.09569931030273
                                },
                                table: 'data_lima_x_punto',
                                radio: 1250,
                                tablehex: 'hex_lima_punto'
                            };
                            var hmtl = '';
                            var hmtlvelo = '';
                            var hmtl_estado = '';
                            var totalvelod = 0;
                            var totalvelol = 0;
                            var totalvelom = 0;
                            var totalvelox = 0;
                            var totalveloj = 0;
                            var totalvelov = 0;
                            var totalvelos = 0;

                            var totald = 0;
                            var totall = 0;
                            var totalm = 0;
                            var totalx = 0;
                            var totalj = 0;
                            var totalv = 0;
                            var totals = 0;
                            $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params_velocidad)), function(response_velo) {
                                console.log(response_velo);
                                if (response_velo) {
                                    $.each(response_velo, function(i, item) {
                                        totalvelod = totalvelod + (item.d == null ? 0 : parseInt(item.d));
                                        totalvelol = totalvelol + (item.l == null ? 0 : parseInt(item.l));
                                        totalvelom = totalvelom + (item.m == null ? 0 : parseInt(item.m));
                                        totalvelox = totalvelox + (item.x == null ? 0 : parseInt(item.x));
                                        totalveloj = totalveloj + (item.j == null ? 0 : parseInt(item.j));
                                        totalvelov = totalvelov + (item.v == null ? 0 : parseInt(item.v));
                                        totalvelos = totalvelos + (item.s == null ? 0 : parseInt(item.s));

                                        hmtlvelo += '<tr>';
                                        hmtlvelo += '<td>' + item.horat + '</td>';
                                        hmtlvelo += '<td class="text-center">' + (item.d == null ? '0' : item.d) + '</td>';
                                        hmtlvelo += '<td class="text-center">' + (item.l == null ? '0' : item.l) + '</td>';
                                        hmtlvelo += '<td class="text-center">' + (item.m == null ? '0' : item.m) + '</td>';
                                        hmtlvelo += '<td class="text-center">' + (item.x == null ? '0' : item.x) + '</td>';
                                        hmtlvelo += '<td class="text-center">' + (item.j == null ? '0' : item.j) + '</td>';
                                        hmtlvelo += '<td class="text-center">' + (item.v == null ? '0' : item.v) + '</td>';
                                        hmtlvelo += '<td class="text-center">' + (item.s == null ? '0' : item.s) + '</td>';
                                        hmtlvelo += '</tr>';
                                    });
                                    hmtlvelo += '<tr>';
                                    hmtlvelo += '<td class="text-center">Total Promedio</td>';
                                    hmtlvelo += '<td class="text-center">' + Math.round(parseInt(totalvelod / 4) / 24) + '</td>';
                                    hmtlvelo += '<td class="text-center">' + Math.round(parseInt(totalvelol / 4) / 24) + '</td>';
                                    hmtlvelo += '<td class="text-center">' + Math.round(parseInt(totalvelom / 4) / 24) + '</td>';
                                    hmtlvelo += '<td class="text-center">' + Math.round(parseInt(totalvelox / 4) / 24) + '</td>';
                                    hmtlvelo += '<td class="text-center">' + Math.round(parseInt(totalveloj / 4) / 24) + '</td>';
                                    hmtlvelo += '<td class="text-center">' + Math.round(parseInt(totalvelov / 4) / 24) + '</td>';
                                    hmtlvelo += '<td class="text-center">' + Math.round(parseInt(totalvelos / 4) / 24) + '</td>';
                                    hmtlvelo += '</tr>';
                                    document.getElementById("content_dashboard").innerHTML = '<div class="row"><div class="col-lg-12"><div class="text-center"><h5><strong>Data Promedio de velocidades >10 km/h y < 100 km/h desde el 26/09/2022 al 02/10/2022 </strong></h5></div><br><table border="0" class="table table-sm table-mariconada"><tr><td class="text-center bold">Hora</td><td class="text-center bold">Dom</td><td class="text-center bold">Lun</td><td class="text-center bold">Mar</td><td class="text-center bold">Mie</td><td class="text-center bold">Jue</td><td class="text-center bold">Vie</td><td class="text-center bold">Sab</td></tr>' + hmtlvelo + '</table></div></div> '
                                    // var popup = L.popup({
                                    //     closeButton: true,
                                    //     minWidth: 900
                                    // }).setLatLng(latlng).setContent(
                                    //     '<div  class="row"><div class="col-lg-6"><div class="text-center"><h5><strong>Data desde el 01/12/2018 al 07/12/2018 </strong></h5></div><br><table border="0" class="table table-sm table-mariconada"><tr><td class="text-center bold">Hora</td><td class="text-center bold">Dom</td><td class="text-center bold">Lun</td><td class="text-center bold">Mar</td><td class="text-center bold">Mie</td><td class="text-center bold">Jue</td><td class="text-center bold">Vie</td><td class="text-center bold">Sab</td></tr>' + hmtl + '</table><div><strong>*La consulta se realizó con la coordenada central:</strong> (' + lat.toString().slice(0, 10) + '; ' + lng.toString().slice(0, 10) + ') <br><strong>y un radio de: </strong>' + Math.round(radio) + ' metros </div></div>'
                                    // ).openOn(map);

                                }
                            });

                            break;
                        default:
                            return
                    }

                }
            });
        }

        function map() {
            map = new L.Map('map', {
                center: [-12.099298, -77.030706],
                zoom: 15,
                zoomControl: false,
                condensedAttributionControl: false
            });

            map.on('click', function(e) {
                // console.log("pane",panelLayer);
                var latlng = e.latlng;
                var layerid = $('.leaflet-panel-layers-group input[type="radio"]:checked');
                if (layerid.length > 0 && $("#chkHexagonos").is(':checked') && flagCircle == 1) {
                    // $("#chkHexagonos").is(':checked')
                    hexagonos_det(layerid.val(), latlng);

                }

                if (map.hasLayer(overLayers.filter(p => p.key == 'ubigeos').shift().layer)) {
                    mapa_calor(latlng);
                }

                // console.log("l", layer);
                var layer = $('.leaflet-panel-layers-group input[type="checkbox"]:checked');
                if (flagCircle == 1 && (map.hasLayer(overLayers.filter(p => p.key == 'delitos_serenos')[0].layers.filter(p => p.key == 'hex_pnp').shift().layer) || map.hasLayer(overLayers.filter(p => p.key == 'delitos_serenos')[0].layers.filter(p => p.key == 'hex_preventivos').shift().layer) || map.hasLayer(overLayers.filter(p => p.key == 'delitos_serenos')[0].layers.filter(p => p.key == 'hex_no_preventivos').shift().layer)) && !$("#chkHexagonos").is(':checked')) {
                    hexagonos_det_pnp_sereno(layer.val(), latlng);
                }

                if (map.hasLayer(overLayers.filter(p => p.key == 'delitosLal').shift().layer) && flagCircle == 1 && !$("#chkHexagonos").is(':checked')) {
                    hexagonos_det_lal(layerid.val(), latlng);
                }

                if (map.hasLayer(overLayers.filter(p => p.key == 'delitosAlto').shift().layer) && flagCircle == 1 && !$("#chkHexagonos").is(':checked')) {
                    hexagonos_det_elalto(layerid.val(), latlng);
                }

                if (map.hasLayer(overLayers.filter(p => p.key == 'delitosaqp').shift().layer) && flagCircle == 1 && !$("#chkHexagonos").is(':checked')) {
                    hexagonos_det_aqp(layerid.val(), latlng);
                }

                if (map.hasLayer(overLayers.filter(p => p.key == 'densidadaqp').shift().layer) && !($('#chkHexagonos').is(':checked'))) {
                    //aqi va cuanod contruya el doble ventana
                    console.log('eeeeee');
                    hexagono_det_densidad('data_aqp_x_punto', 'hex_aqp_punto', latlng, 1);
                }

            });

            map.addLayer(new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'));

        }

        function hexagono_det_densidad(tabla, tablahex, latlng, radio) {
            var latlngc = latlng;
            var hmtl = '';
            var hmtlvelo = '';
            var hmtl_estado = '';
            var ll = latlng;
            var lat = ll.lat;
            var lng = ll.lng;
            var totalvelod = 0;
            var totalvelol = 0;
            var totalvelom = 0;
            var totalvelox = 0;
            var totalveloj = 0;
            var totalvelov = 0;
            var totalvelos = 0;

            var totald = 0;
            var totall = 0;
            var totalm = 0;
            var totalx = 0;
            var totalj = 0;
            var totalv = 0;
            var totals = 0;
            var params = {
                method: 'fs_analisis_det_densidad',
                lanlng: latlng,
                table: tabla,
                radio: radio,
                tablehex: tablahex
            };
            var params_velocidad = {
                method: 'fs_analisis_velo',
                lanlng: latlng,
                table: tabla,
                radio: radio,
                tablehex: tablahex
            };
            var params_estado = {
                method: 'fs_analisis_det_tabla_v3',
                lanlng: latlng,
                table: tabla,
                radio: radio,
                tablehex: tablahex
            };
            console.log(latlngc);

            if (latlngc.toString.length > 0) {
                // console.log('aquiii');
                //    $('#map').css('cursor', 'progress');
                $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params_estado)), function(response_estado) {
                    if (response_estado.length > 0) {
                        $.each(response_estado, function(i, item) {
                            hmtl_estado += '<tr>';
                            hmtl_estado += '<td>' + item.clasifica + '</td>';
                            hmtl_estado += '<td class="text-right">' + item.cant + '</td>';
                            hmtl_estado += '</tr>';
                        });
                    }
                });
                $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params_velocidad)), function(response_velo) {

                    console.log(response_velo);
                    if (response_velo) {
                        $.each(response_velo, function(i, item) {
                            totalvelod = totalvelod + (item.d == null ? 0 : parseInt(item.d));
                            totalvelol = totalvelol + (item.l == null ? 0 : parseInt(item.l));
                            totalvelom = totalvelom + (item.m == null ? 0 : parseInt(item.m));
                            totalvelox = totalvelox + (item.x == null ? 0 : parseInt(item.x));
                            totalveloj = totalveloj + (item.j == null ? 0 : parseInt(item.j));
                            totalvelov = totalvelov + (item.v == null ? 0 : parseInt(item.v));
                            totalvelos = totalvelos + (item.s == null ? 0 : parseInt(item.s));

                            hmtlvelo += '<tr>';
                            hmtlvelo += '<td class="text-center">' + item.horat + '</td>';
                            hmtlvelo += '<td class="text-center">' + (item.d == null ? '0' : item.d) + '</td>';
                            hmtlvelo += '<td class="text-center">' + (item.l == null ? '0' : item.l) + '</td>';
                            hmtlvelo += '<td class="text-center">' + (item.m == null ? '0' : item.m) + '</td>';
                            hmtlvelo += '<td class="text-center">' + (item.x == null ? '0' : item.x) + '</td>';
                            hmtlvelo += '<td class="text-center">' + (item.j == null ? '0' : item.j) + '</td>';
                            hmtlvelo += '<td class="text-center">' + (item.v == null ? '0' : item.v) + '</td>';
                            hmtlvelo += '<td class="text-center">' + (item.s == null ? '0' : item.s) + '</td>';
                            hmtlvelo += '</tr>';
                        });
                        hmtlvelo += '<tr>';
                        hmtlvelo += '<td class="text-center">Total Promedio</td>';
                        hmtlvelo += '<td class="text-center">' + Math.round(totalvelod / 24) + '</td>';
                        hmtlvelo += '<td class="text-center">' + Math.round(totalvelol / 24) + '</td>';
                        hmtlvelo += '<td class="text-center">' + Math.round(totalvelom / 24) + '</td>';
                        hmtlvelo += '<td class="text-center">' + Math.round(totalvelox / 24) + '</td>';
                        hmtlvelo += '<td class="text-center">' + Math.round(totalveloj / 24) + '</td>';
                        hmtlvelo += '<td class="text-center">' + Math.round(totalvelov / 24) + '</td>';
                        hmtlvelo += '<td class="text-center">' + Math.round(totalvelos / 24) + '</td>';
                        hmtlvelo += '</tr>';
                        // var popup = L.popup({
                        //     closeButton: true,
                        //     minWidth: 900
                        // }).setLatLng(latlng).setContent(
                        //     '<div  class="row"><div class="col-lg-6"><div class="text-center"><h5><strong>Data desde el 01/12/2018 al 07/12/2018 </strong></h5></div><br><table border="0" class="table table-sm table-mariconada"><tr><td class="text-center bold">Hora</td><td class="text-center bold">Dom</td><td class="text-center bold">Lun</td><td class="text-center bold">Mar</td><td class="text-center bold">Mie</td><td class="text-center bold">Jue</td><td class="text-center bold">Vie</td><td class="text-center bold">Sab</td></tr>' + hmtl + '</table><div><strong>*La consulta se realizó con la coordenada central:</strong> (' + lat.toString().slice(0, 10) + '; ' + lng.toString().slice(0, 10) + ') <br><strong>y un radio de: </strong>' + Math.round(radio) + ' metros </div></div>'
                        // ).openOn(map);

                    }
                
                });
                $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params)), function(response) {
                        // console.log(response);
                        // console.log('tooo');

                        if (response) {
                            $.each(response, function(i, item) {
                                totald = totald + (item.d == null ? 0 : parseInt(item.d));
                                totall = totall + (item.l == null ? 0 : parseInt(item.l));
                                totalm = totalm + (item.m == null ? 0 : parseInt(item.m));
                                totalx = totalx + (item.x == null ? 0 : parseInt(item.x));
                                totalj = totalj + (item.j == null ? 0 : parseInt(item.j));
                                totalv = totalv + (item.v == null ? 0 : parseInt(item.v));
                                totals = totals + (item.s == null ? 0 : parseInt(item.s));

                                hmtl += '<tr>';
                                hmtl += '<td class="text-center">' + item.horat + '</td>';
                                hmtl += '<td class="text-center">' + (item.d == null ? '0' : item.d) + '</td>';
                                hmtl += '<td class="text-center">' + (item.l == null ? '0' : item.l) + '</td>';
                                hmtl += '<td class="text-center">' + (item.m == null ? '0' : item.m) + '</td>';
                                hmtl += '<td class="text-center">' + (item.x == null ? '0' : item.x) + '</td>';
                                hmtl += '<td class="text-center">' + (item.j == null ? '0' : item.j) + '</td>';
                                hmtl += '<td class="text-center">' + (item.v == null ? '0' : item.v) + '</td>';
                                hmtl += '<td class="text-center">' + (item.s == null ? '0' : item.s) + '</td>';
                                hmtl += '</tr>';
                            });
                            hmtl += '<tr>';
                            hmtl += '<td class="text-center">Total Promedio</td>';
                            hmtl += '<td class="text-center">' + Math.round(totald / 24) + '</td>';
                            hmtl += '<td class="text-center">' + Math.round(totall / 24) + '</td>';
                            hmtl += '<td class="text-center">' + Math.round(totalm / 24) + '</td>';
                            hmtl += '<td class="text-center">' + Math.round(totalx / 24) + '</td>';
                            hmtl += '<td class="text-center">' + Math.round(totalj / 24) + '</td>';
                            hmtl += '<td class="text-center">' + Math.round(totalv / 24) + '</td>';
                            hmtl += '<td class="text-center">' + Math.round(totals / 24) + '</td>';
                            hmtl += '</tr>';
                            var popup = L.popup({
                                closeButton: true,
                                minWidth: 900
                            }).setLatLng(latlng).setContent(
                                '<div class="row"><div class="col-lg-6"><div class="text-center"><h5><strong>Data Densidad vehicular desde el 01/12/2018 al 07/12/2018 </strong></h5></div><br><br><table border="0" class="table table-sm table-mariconada"><tr><td class="text-center bold">Hora</td><td class="text-center bold">Dom</td><td class="text-center bold">Lun</td><td class="text-center bold">Mar</td><td class="text-center bold">Mie</td><td class="text-center bold">Jue</td><td class="text-center bold">Vie</td><td class="text-center bold">Sab</td></tr>' + hmtl + '</table></div> <div class="col-lg-6"><div class="text-center"><h5><strong>Data Promedio de velocidades >10 km/h y < 100 km/h desde el 01/12/2018 al 07/12/2018 </strong></h5></div><br><table border="0" class="table table-sm table-mariconada"><tr><td class="text-center bold">Hora</td><td class="text-center bold">Dom</td><td class="text-center bold">Lun</td><td class="text-center bold">Mar</td><td class="text-center bold">Mie</td><td class="text-center bold">Jue</td><td class="text-center bold">Vie</td><td class="text-center bold">Sab</td></tr>' + hmtlvelo + '</table></div></div> <div><table border="0" class="table table-sm table-mariconada"><tr><td class="text-center bold">GEOCERCAS</td><td class="text-center bold">CANTIDAD</td></tr>' + hmtl_estado + '</table></div> <div> <strong>*La consulta se realizó con la coordenada central:</strong> (' + lat.toString().slice(0, 10) + '; ' + lng.toString().slice(0, 10) + ')<strong>y un radio de: </strong>' + Math.round(radio) + ' metros </div>'
                            ).openOn(map);

                        }

                        // $('#map').css('cursor', 'pointer');
                    });

            }


        }


        function hexagono_det_(layerid, latlng, radio) {

            var hmtl = '';
            var ll = latlng;
            var lat = ll.lat;
            var lng = ll.lng;
            // console.log(lat.toString().slice(0,10));
            var parent = baseLayers.filter(p => p.key == 'ghexagonos').shift().layers.filter(p => p.id == layerid).shift().parent;
            var baselayer = overLayers.filter(p => p.group == 'mask_hexagonos').shift().layers.filter(p => p.key == parent).shift().table;
            var params = {
                method: 'fs_analisis_det_v2',
                lanlng: latlng,
                table: baselayer,
                radio: radio
            };
            var params_estado = {
                method: 'fs_analisis_det_tabla_v2',
                lanlng: latlng,
                table: baselayer,
                radio: radio
            };
            // console.log(params);
            var hmtl_estado = '';
            if (baselayer.length > 0) {
                $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params_estado)), function(response_estado) {
                    if (response_estado.length > 0) {
                        $.each(response_estado, function(i, item) {
                            hmtl_estado += '<tr>';
                            hmtl_estado += '<td>' + item.clasifica + '</td>';
                            hmtl_estado += '<td class="text-right">' + item.cant + '</td>';
                            hmtl_estado += '</tr>';
                        });
                    }
                    $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params)), function(response) {
                        if (response.length > 0) {
                            $.each(response, function(i, item) {
                                hmtl += '<tr>';
                                hmtl += '<td>' + item.generico + '</td>';
                                hmtl += '<td class="text-right">' + item.cant + '</td>';
                                hmtl += '</tr>';
                            });
                            var popup = L.popup({
                                closeButton: true,
                                minWidth: 450
                            }).setLatLng(latlng).setContent(
                                `<div id="analisis_det_chartv2" style="min-height: 400px;display: flex; align-items: center; justify-content: center;">
                                <img src="img/200.gif"/></div>
                                <br>
                                <div>
                                    <table border="0" class="table table-sm table-mariconada">
                                    <tr>
                                        <td class="text-center bold">MODALIDAD DE DELITO</td>
                                        <td class="text-center bold">CANTIDAD</td>
                                    </tr>` + hmtl + `</table>
                                    <table border="0" class="table table-sm table-mariconada">
                                    <tr>
                                        <td class="text-center bold">GEOCERCAS</td>
                                        <td class="text-center bold">CANTIDAD</td>
                                        </tr>` + hmtl_estado + `</table>
                                </div>
                                <div>
                                    <strong>*La consulta se realizó con la coordenada central:</strong> (` + lat.toString().slice(0, 10) + `; ` + lng.toString().slice(0, 10) + `) <br><strong>y un radio de: </strong>' + Math.round(radio) + ' metros </div>`
                            ).openOn(map);
                            fs_grafico({
                                method: 'fs_analisis_det_chart_v2',
                                grafico: 'analisis_det_chartv2',
                                lanlng: latlng,
                                table: baselayer,
                                radio: radio
                            });
                        }
                        $('#map').css('cursor', 'pointer');
                    });
                });

            }
        }

        function hexagono_det_aqp(latlng, radio) {

            var hmtl = '';
            var ll = latlng;
            var lat = ll.lat;
            var lng = ll.lng;
            // console.log(lat.toString().slice(0,10));
            var params = {
                method: 'fs_analisis_det_aqp',
                lanlng: latlng,
                radio: radio
            };
            var params_estado = {
                method: 'fs_analisis_det_tabla_aqp',
                lanlng: latlng,
                radio: radio
            };
            // console.log(params);
            var hmtl_estado2 = '';

            $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params_estado)), function(response_estado) {

                // console.log(response_estado);
                $.each(response_estado, function(i, item) {
                    // console.log(item);
                    hmtl_estado2 += '<tr>';
                    hmtl_estado2 += '<td>' + item.clasifica + '</td>';
                    hmtl_estado2 += '<td class="text-right">' + item.cant + '</td>';
                    hmtl_estado2 += '</tr>';
                });
                $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params)), function(response) {
                    if (response.length > 0) {
                        $.each(response, function(i, item) {
                            hmtl += '<tr>';
                            hmtl += '<td>' + item.generico + '</td>';
                            hmtl += '<td class="text-right">' + item.cant + '</td>';
                            hmtl += '</tr>';
                        });
                        var popup = L.popup({
                            closeButton: true,
                            minWidth: 900
                        }).setLatLng(latlng).setContent(
                            '<div id="analisis_det_chartv2" style="min-height: 400px;display: flex; align-items: center; justify-content: center;"><img src="img/200.gif"/></div><br ><div><table border="0" class="table table-sm table-mariconada"><tr><td class="text-center bold">MODALIDAD DE DELITO</td><td class="text-center bold">CANTIDAD</td></tr>' + hmtl + '</table><table border="0" class="table table-sm table-mariconada"><tr><td class="text-center bold">GEOCERCAS</td><td class="text-center bold">CANTIDAD</td></tr>' + hmtl_estado2 + '</table></div><div><strong>*La consulta se realizó con la coordenada central:</strong> (' + lat.toString().slice(0, 10) + '; ' + lng.toString().slice(0, 10) + ') <br><strong>y un radio de: </strong>' + Math.round(radio) + ' metros </div>'
                        ).openOn(map);
                        fs_grafico({
                            method: 'fs_analisis_det_chart_v2_aqp',
                            grafico: 'analisis_det_chartv2',
                            lanlng: latlng,
                            table: '',
                            radio: radio
                        });
                    }
                    $('#map').css('cursor', 'pointer');
                });

            });


        }

        /**La libertad */
        function hexagonos_det_lal(layerid, latlng) {
            var hmtl = '';
            var params = {
                method: 'fs_analisis_det_lal',
                lanlng: latlng
            };
            if (1) {
                /*const chart = await fetch('php/app.php?data=' + encodeURIComponent(JSON.stringify({...params, method: 'fs_analisis_det_chart'})));				
                console.log(await chart.json());*/

                $('#map').css('cursor', 'progress');

                $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params)), function(response) {
                    if (response.length > 0) {
                        $.each(response, function(i, item) {
                            hmtl += '<tr>';
                            hmtl += '<td class="text-left">' + item.subcategoria + '</td>';
                            hmtl += '<td class="text-center">' + Number(item.a2016) + '</td>';
                            hmtl += '<td class="text-center">' + Number(item.a2017) + '</td>';
                            hmtl += '<td class="text-center">' + Number(item.a2018) + '</td>';
                            hmtl += '<td class="text-center">' + Number(item.a2019) + '</td>';
                            hmtl += '<td class="text-center">' + Number(item.a2020) + '</td>';
                            hmtl += '<td class="text-center">' + Number(item.a2021) + '</td>';
                            hmtl += '<td class="text-center">' + Number(item.cant) + '</td>';
                            hmtl += '</tr>';
                        });

                        hmtl += '<tr class="table-total">';
                        hmtl += '<td class="text-center bold">Total</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2016
                        }) => acc + Number(a2016), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2017
                        }) => acc + Number(a2017), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2018
                        }) => acc + Number(a2018), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2019
                        }) => acc + Number(a2019), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2020
                        }) => acc + Number(a2020), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2021
                        }) => acc + Number(a2021), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            cant
                        }) => acc + Number(cant), 0) + '</td>';
                        hmtl += '</tr>';

                        var popup = L.popup({
                            closeButton: true,
                            minWidth: 900
                        }).setLatLng(latlng).setContent(
                            `<div id="analisis_det_lal_chart" style="min-height: 400px;display: flex; align-items: center; justify-content: center;">
                            <img src="img/200.gif"/></div>
                            <table border="0" class="table table-sm table-mariconada">
                            <tr>
                                <td class="text-center bold">MODALIDAD DE DELITO</td>
                                <td class="text-center bold">2016</td>
                                <td class="text-center bold">2017</td>
                                <td class="text-center bold">2018</td>
                                <td class="text-center bold">2019</td>
                                <td class="text-center bold">2020</td>
                                <td class="text-center bold">2021</td>
                                <td class="text-center bold">CANTIDAD</td>
                                </tr>` + hmtl + `</table>`
                        ).openOn(map);

                        fs_grafico({
                            method: 'fs_analisis_det_lal_chart',
                            grafico: 'analisis_det_lal_chart',
                            lanlng: latlng
                        });
                    }

                    $('#map').css('cursor', 'pointer');
                });
            }
        }

        function hexagono_det_lal_radio(layerid, latlng, radio) {

            var hmtl = '';
            var ll = latlng;
            var lat = ll.lat;
            var lng = ll.lng;
            // console.log(lat.toString().slice(0,10));
            var params = {
                method: 'fs_analisis_det_lal_radio',
                lanlng: latlng,
                radio: radio
            };
            var params_estado = {
                method: 'fs_analisis_det_lal_radio_estado',
                lanlng: latlng,
                radio: radio
            };
            // console.log(params);
            var hmtl_estado = '';
            if (1) {
                $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params_estado)), function(response_estado) {
                    if (response_estado.length > 0) {
                        $.each(response_estado, function(i, item) {
                            hmtl_estado += '<tr>';
                            hmtl_estado += '<td>' + item.clasifica + '</td>';
                            hmtl_estado += '<td class="text-right">' + item.cant + '</td>';
                            hmtl_estado += '</tr>';
                        });
                    }
                    $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params)), function(response) {
                        if (response.length > 0) {
                            $.each(response, function(i, item) {
                                hmtl += '<tr>';
                                hmtl += '<td class="text-left">' + item.subcategoria + '</td>';
                                hmtl += '<td class="text-center">' + Number(item.a2016) + '</td>';
                                hmtl += '<td class="text-center">' + Number(item.a2017) + '</td>';
                                hmtl += '<td class="text-center">' + Number(item.a2018) + '</td>';
                                hmtl += '<td class="text-center">' + Number(item.a2019) + '</td>';
                                hmtl += '<td class="text-center">' + Number(item.a2020) + '</td>';
                                hmtl += '<td class="text-center">' + Number(item.a2021) + '</td>';
                                hmtl += '<td class="text-center">' + Number(item.cant) + '</td>';
                                hmtl += '</tr>';
                            });

                            hmtl += '<tr class="table-total">';
                            hmtl += '<td class="text-center bold">Total</td>';
                            hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                                a2016
                            }) => acc + Number(a2016), 0) + '</td>';
                            hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                                a2017
                            }) => acc + Number(a2017), 0) + '</td>';
                            hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                                a2018
                            }) => acc + Number(a2018), 0) + '</td>';
                            hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                                a2019
                            }) => acc + Number(a2019), 0) + '</td>';
                            hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                                a2020
                            }) => acc + Number(a2020), 0) + '</td>';
                            hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                                a2021
                            }) => acc + Number(a2021), 0) + '</td>';
                            hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                                cant
                            }) => acc + Number(cant), 0) + '</td>';
                            hmtl += '</tr>';

                            var popup = L.popup({
                                closeButton: true,
                                minWidth: 900
                            }).setLatLng(latlng).setContent(
                                `<div id="analisis_det_lal_radio_chart" style="min-height: 400px;display: flex; align-items: center; justify-content: center;">
                            <img src="img/200.gif"/></div>
                            <table border="0" class="table table-sm table-mariconada">
                            <tr>
                                <td class="text-center bold">MODALIDAD DE DELITO</td>
                                <td class="text-center bold">2016</td>
                                <td class="text-center bold">2017</td>
                                <td class="text-center bold">2018</td>
                                <td class="text-center bold">2019</td>
                                <td class="text-center bold">2020</td>
                                <td class="text-center bold">2021</td>
                                <td class="text-center bold">CANTIDAD</td>
                                </tr>` + hmtl + `</table>
                                <table border="0" class="table table-sm table-mariconada">
                                    <tr>
                                        <td class="text-center bold">GEOCERCAS</td>
                                        <td class="text-center bold">CANTIDAD</td>
                                        </tr>` + hmtl_estado + `</table>` +
                                `<div>
                                    <strong>*La consulta se realizó con la coordenada central:</strong> (` + lat.toString().slice(0, 10) + `; ` + lng.toString().slice(0, 10) + `) <br><strong>y un radio de: </strong>' + Math.round(radio) + ' metros </div>`

                            ).openOn(map);
                            fs_grafico({
                                method: 'fs_analisis_det_lal_radio_chart',
                                grafico: 'analisis_det_lal_radio_chart',
                                lanlng: latlng,
                                radio: radio
                            });
                        }
                        $('#map').css('cursor', 'pointer');
                    });
                });

            }
        }
        /**fin la liberdad */

        /**Arequipa */
        function hexagonos_det_aqp(layerid, latlng) {
            var hmtl = '';
            var params = {
                method: 'fs_analisis_det_aqp',
                lanlng: latlng
            };
            if (1) {
                /*const chart = await fetch('php/app.php?data=' + encodeURIComponent(JSON.stringify({...params, method: 'fs_analisis_det_chart'})));				
                console.log(await chart.json());*/

                $('#map').css('cursor', 'progress');

                $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params)), function(response) {
                    if (response.length > 0) {
                        $.each(response, function(i, item) {
                            hmtl += '<tr>';
                            hmtl += '<td class="text-left">' + item.subcategoria + '</td>';
                            hmtl += '<td class="text-center">' + Number(item.a2016) + '</td>';
                            hmtl += '<td class="text-center">' + Number(item.a2017) + '</td>';
                            hmtl += '<td class="text-center">' + Number(item.a2018) + '</td>';
                            hmtl += '<td class="text-center">' + Number(item.a2019) + '</td>';
                            hmtl += '<td class="text-center">' + Number(item.a2020) + '</td>';
                            hmtl += '<td class="text-center">' + Number(item.a2021) + '</td>';
                            hmtl += '<td class="text-center">' + Number(item.cant) + '</td>';
                            hmtl += '</tr>';
                        });

                        hmtl += '<tr class="table-total">';
                        hmtl += '<td class="text-center bold">Total</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2016
                        }) => acc + Number(a2016), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2017
                        }) => acc + Number(a2017), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2018
                        }) => acc + Number(a2018), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2019
                        }) => acc + Number(a2019), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2020
                        }) => acc + Number(a2020), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2021
                        }) => acc + Number(a2021), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            cant
                        }) => acc + Number(cant), 0) + '</td>';
                        hmtl += '</tr>';

                        var popup = L.popup({
                            closeButton: true,
                            minWidth: 900
                        }).setLatLng(latlng).setContent(
                            `<div id="analisis_det_aqp_chart" style="min-height: 400px;display: flex; align-items: center; justify-content: center;">
                            <img src="img/200.gif"/></div>
                            <table border="0" class="table table-sm table-mariconada">
                            <tr>
                                <td class="text-center bold">MODALIDAD DE DELITO</td>
                                <td class="text-center bold">2016</td>
                                <td class="text-center bold">2017</td>
                                <td class="text-center bold">2018</td>
                                <td class="text-center bold">2019</td>
                                <td class="text-center bold">2020</td>
                                <td class="text-center bold">2021</td>
                                <td class="text-center bold">CANTIDAD</td>
                                </tr>` + hmtl + `</table>`
                        ).openOn(map);

                        fs_grafico({
                            method: 'fs_analisis_det_aqp_chart',
                            grafico: 'analisis_det_aqp_chart',
                            lanlng: latlng
                        });
                    }

                    $('#map').css('cursor', 'pointer');
                });
            }
        }

        function hexagono_det_aqp_radio(layerid, latlng, radio) {

            var hmtl = '';
            var ll = latlng;
            var lat = ll.lat;
            var lng = ll.lng;
            // console.log(lat.toString().slice(0,10));
            var params = {
                method: 'fs_analisis_det_aqp_radio',
                lanlng: latlng,
                radio: radio
            };
            var params_estado = {
                method: 'fs_analisis_det_aqp_radio_estado',
                lanlng: latlng,
                radio: radio
            };
            // console.log(params);
            var hmtl_estado = '';
            if (1) {
                $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params_estado)), function(response_estado) {
                    if (response_estado.length > 0) {
                        $.each(response_estado, function(i, item) {
                            hmtl_estado += '<tr>';
                            hmtl_estado += '<td>' + item.clasifica + '</td>';
                            hmtl_estado += '<td class="text-right">' + item.cant + '</td>';
                            hmtl_estado += '</tr>';
                        });
                    }
                    $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params)), function(response) {
                        if (response.length > 0) {
                            $.each(response, function(i, item) {
                                hmtl += '<tr>';
                                hmtl += '<td class="text-left">' + item.subcategoria + '</td>';
                                hmtl += '<td class="text-center">' + Number(item.a2016) + '</td>';
                                hmtl += '<td class="text-center">' + Number(item.a2017) + '</td>';
                                hmtl += '<td class="text-center">' + Number(item.a2018) + '</td>';
                                hmtl += '<td class="text-center">' + Number(item.a2019) + '</td>';
                                hmtl += '<td class="text-center">' + Number(item.a2020) + '</td>';
                                hmtl += '<td class="text-center">' + Number(item.a2021) + '</td>';
                                hmtl += '<td class="text-center">' + Number(item.cant) + '</td>';
                                hmtl += '</tr>';
                            });

                            hmtl += '<tr class="table-total">';
                            hmtl += '<td class="text-center bold">Total</td>';
                            hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                                a2016
                            }) => acc + Number(a2016), 0) + '</td>';
                            hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                                a2017
                            }) => acc + Number(a2017), 0) + '</td>';
                            hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                                a2018
                            }) => acc + Number(a2018), 0) + '</td>';
                            hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                                a2019
                            }) => acc + Number(a2019), 0) + '</td>';
                            hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                                a2020
                            }) => acc + Number(a2020), 0) + '</td>';
                            hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                                a2021
                            }) => acc + Number(a2021), 0) + '</td>';
                            hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                                cant
                            }) => acc + Number(cant), 0) + '</td>';
                            hmtl += '</tr>';

                            var popup = L.popup({
                                closeButton: true,
                                minWidth: 900
                            }).setLatLng(latlng).setContent(
                                `<div id="analisis_det_aqp_radio_chart" style="min-height: 400px;display: flex; align-items: center; justify-content: center;">
                            <img src="img/200.gif"/></div>
                            <table border="0" class="table table-sm table-mariconada">
                            <tr>
                                <td class="text-center bold">MODALIDAD DE DELITO</td>
                                <td class="text-center bold">2016</td>
                                <td class="text-center bold">2017</td>
                                <td class="text-center bold">2018</td>
                                <td class="text-center bold">2019</td>
                                <td class="text-center bold">2020</td>
                                <td class="text-center bold">2021</td>
                                <td class="text-center bold">CANTIDAD</td>
                                </tr>` + hmtl + `</table>
                                <table border="0" class="table table-sm table-mariconada">
                                    <tr>
                                        <td class="text-center bold">GEOCERCAS</td>
                                        <td class="text-center bold">CANTIDAD</td>
                                        </tr>` + hmtl_estado + `</table>` +
                                `<div>
                                    <strong>*La consulta se realizó con la coordenada central:</strong> (` + lat.toString().slice(0, 10) + `; ` + lng.toString().slice(0, 10) + `) <br><strong>y un radio de: </strong>` + Math.round(radio) + ` metros </div>`

                            ).openOn(map);
                            fs_grafico({
                                method: 'fs_analisis_det_aqp_radio_chart',
                                grafico: 'analisis_det_aqp_radio_chart',
                                lanlng: latlng,
                                radio: radio
                            });
                        }
                        $('#map').css('cursor', 'pointer');
                    });
                });

            }
        }
        /**fin Arequipa */

        /**El alto */
        function hexagonos_det_elalto(layerid, latlng) {
            var hmtl = '';
            var params = {
                method: 'fs_analisis_det_elalto',
                lanlng: latlng
            };
            if (1) {
                /*const chart = await fetch('php/app.php?data=' + encodeURIComponent(JSON.stringify({...params, method: 'fs_analisis_det_chart'})));				
                console.log(await chart.json());*/

                $('#map').css('cursor', 'progress');

                $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params)), function(response) {
                    if (response.length > 0) {
                        $.each(response, function(i, item) {
                            hmtl += '<tr>';
                            hmtl += '<td class="text-left">' + item.subcategoria + '</td>';
                            hmtl += '<td class="text-center">' + Number(item.a2016) + '</td>';
                            hmtl += '<td class="text-center">' + Number(item.a2017) + '</td>';
                            hmtl += '<td class="text-center">' + Number(item.a2018) + '</td>';
                            hmtl += '<td class="text-center">' + Number(item.a2019) + '</td>';
                            hmtl += '<td class="text-center">' + Number(item.a2020) + '</td>';
                            hmtl += '<td class="text-center">' + Number(item.a2021) + '</td>';
                            hmtl += '<td class="text-center">' + Number(item.cant) + '</td>';
                            hmtl += '</tr>';
                        });

                        hmtl += '<tr class="table-total">';
                        hmtl += '<td class="text-center bold">Total</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2016
                        }) => acc + Number(a2016), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2017
                        }) => acc + Number(a2017), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2018
                        }) => acc + Number(a2018), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2019
                        }) => acc + Number(a2019), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2020
                        }) => acc + Number(a2020), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2021
                        }) => acc + Number(a2021), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            cant
                        }) => acc + Number(cant), 0) + '</td>';
                        hmtl += '</tr>';

                        var popup = L.popup({
                            closeButton: true,
                            minWidth: 900
                        }).setLatLng(latlng).setContent(
                            `<div id="analisis_det_elalto_chart" style="min-height: 400px;display: flex; align-items: center; justify-content: center;">
                            <img src="img/200.gif"/></div>
                            <table border="0" class="table table-sm table-mariconada">
                            <tr>
                                <td class="text-center bold">MODALIDAD DE DELITO</td>
                                <td class="text-center bold">2016</td>
                                <td class="text-center bold">2017</td>
                                <td class="text-center bold">2018</td>
                                <td class="text-center bold">2019</td>
                                <td class="text-center bold">2020</td>
                                <td class="text-center bold">2021</td>
                                <td class="text-center bold">CANTIDAD</td>
                                </tr>` + hmtl + `</table>`
                        ).openOn(map);

                        fs_grafico({
                            method: 'fs_analisis_det_elalto_chart',
                            grafico: 'analisis_det_elalto_chart',
                            lanlng: latlng
                        });
                    }

                    $('#map').css('cursor', 'pointer');
                });
            }
        }

        function hexagono_det_elalto_radio(layerid, latlng, radio) {

            var hmtl = '';
            var ll = latlng;
            var lat = ll.lat;
            var lng = ll.lng;
            // console.log(lat.toString().slice(0,10));
            var params = {
                method: 'fs_analisis_det_elalto_radio',
                lanlng: latlng,
                radio: radio
            };
            var params_estado = {
                method: 'fs_analisis_det_elalto_radio_estado',
                lanlng: latlng,
                radio: radio
            };
            // console.log(params);
            var hmtl_estado = '';
            if (1) {
                $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params_estado)), function(response_estado) {
                    if (response_estado.length > 0) {
                        $.each(response_estado, function(i, item) {
                            hmtl_estado += '<tr>';
                            hmtl_estado += '<td>' + item.clasifica + '</td>';
                            hmtl_estado += '<td class="text-right">' + item.cant + '</td>';
                            hmtl_estado += '</tr>';
                        });
                    }
                    $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params)), function(response) {
                        if (response.length > 0) {
                            $.each(response, function(i, item) {
                                hmtl += '<tr>';
                                hmtl += '<td class="text-left">' + item.subcategoria + '</td>';
                                hmtl += '<td class="text-center">' + Number(item.a2016) + '</td>';
                                hmtl += '<td class="text-center">' + Number(item.a2017) + '</td>';
                                hmtl += '<td class="text-center">' + Number(item.a2018) + '</td>';
                                hmtl += '<td class="text-center">' + Number(item.a2019) + '</td>';
                                hmtl += '<td class="text-center">' + Number(item.a2020) + '</td>';
                                hmtl += '<td class="text-center">' + Number(item.a2021) + '</td>';
                                hmtl += '<td class="text-center">' + Number(item.cant) + '</td>';
                                hmtl += '</tr>';
                            });

                            hmtl += '<tr class="table-total">';
                            hmtl += '<td class="text-center bold">Total</td>';
                            hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                                a2016
                            }) => acc + Number(a2016), 0) + '</td>';
                            hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                                a2017
                            }) => acc + Number(a2017), 0) + '</td>';
                            hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                                a2018
                            }) => acc + Number(a2018), 0) + '</td>';
                            hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                                a2019
                            }) => acc + Number(a2019), 0) + '</td>';
                            hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                                a2020
                            }) => acc + Number(a2020), 0) + '</td>';
                            hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                                a2021
                            }) => acc + Number(a2021), 0) + '</td>';
                            hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                                cant
                            }) => acc + Number(cant), 0) + '</td>';
                            hmtl += '</tr>';

                            var popup = L.popup({
                                closeButton: true,
                                minWidth: 900
                            }).setLatLng(latlng).setContent(
                                `<div id="analisis_det_elalto_radio_chart" style="min-height: 400px;display: flex; align-items: center; justify-content: center;">
                            <img src="img/200.gif"/></div>
                            <table border="0" class="table table-sm table-mariconada">
                            <tr>
                                <td class="text-center bold">MODALIDAD DE DELITO</td>
                                <td class="text-center bold">2016</td>
                                <td class="text-center bold">2017</td>
                                <td class="text-center bold">2018</td>
                                <td class="text-center bold">2019</td>
                                <td class="text-center bold">2020</td>
                                <td class="text-center bold">2021</td>
                                <td class="text-center bold">CANTIDAD</td>
                                </tr>` + hmtl + `</table>
                                <table border="0" class="table table-sm table-mariconada">
                                    <tr>
                                        <td class="text-center bold">GEOCERCAS</td>
                                        <td class="text-center bold">CANTIDAD</td>
                                        </tr>` + hmtl_estado + `</table>` +
                                `<div>
                                    <strong>*La consulta se realizó con la coordenada central:</strong> (` + lat.toString().slice(0, 10) + `; ` + lng.toString().slice(0, 10) + `) <br><strong>y un radio de: </strong>` + Math.round(radio) + ` metros </div>`

                            ).openOn(map);
                            fs_grafico({
                                method: 'fs_analisis_det_elalto_radio_chart',
                                grafico: 'analisis_det_elalto_radio_chart',
                                lanlng: latlng,
                                radio: radio
                            });
                        }
                        $('#map').css('cursor', 'pointer');
                    });
                });

            }
        }
        /**fin el alto */
        function hexagono_det_doble(layerid, latlng, radio) {

            var hmtl = '';
            var ll = latlng;
            var lat = ll.lat;
            var lng = ll.lng;

            var latlngc = latlng;
            var hmtlv2 = '';
            var ll = latlng;
            var lat = ll.lat;
            var lng = ll.lng;
            var totald = 0;
            var totall = 0;
            var totalm = 0;
            var totalx = 0;
            var totalj = 0;
            var totalv = 0;
            var totals = 0;
            // console.log(lat.toString().slice(0,10));
            var parent = baseLayers.filter(p => p.key == 'ghexagonos').shift().layers.filter(p => p.id == layerid).shift().parent;
            var baselayer = overLayers.filter(p => p.group == 'mask_hexagonos').shift().layers.filter(p => p.key == parent).shift().table;
            var params = {
                method: 'fs_analisis_det_v2',
                lanlng: latlng,
                table: baselayer,
                radio: radio
            };
            var params_estado = {
                method: 'fs_analisis_det_tabla_v2',
                lanlng: latlng,
                table: baselayer,
                radio: radio
            };
            var params_doble = {
                method: 'fs_analisis_det_densidad',
                lanlng: latlng,
                table: '',
                radio: radio
            };
            // console.log(params);
            var hmtl_estado = '';
            if (baselayer.length > 0) {
                // if (latlngc.toString.length > 0) {
                // console.log('aquiii');
                //    $('#map').css('cursor', 'progress');
                $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params_doble)), function(response_v2) {
                    // console.log(response);
                    // console.log('tooo');
                    console.log(response_v2);

                    $.each(response_v2, function(i, item) {
                        console.log(item);
                        totald = totald + (item.d == null ? (parseInt(item.l == null ? 0 : item.l) + parseInt(item.l == null ? 0 / 2 : item.l / 2)) : item.d);
                        totall = totall + (item.l == null ? 0 : parseInt(item.l));
                        totalm = totalm + (item.m == null ? 0 : parseInt(item.m));
                        totalx = totalx + (item.x == null ? (parseInt(item.j == null ? 0 : item.j) + parseInt(item.j == null ? 0 / 2 : item.j / 2)) : item.x);
                        totalj = totalj + (item.j == null ? 0 : parseInt(item.j));
                        totalv = totalv + (item.v == null ? 0 : parseInt(item.v));
                        totals = totals + (item.s == null ? 0 : parseInt(item.s));

                        hmtlv2 += '<tr>';
                        hmtlv2 += '<td>' + item.horat + '</td>';
                        hmtlv2 += '<td class="text-center">' + (item.d == null ? (parseInt(item.l == null ? 0 : item.l) + parseInt(item.l == null ? 0 / 2 : item.l / 2)) : item.d) + '</td>';
                        hmtlv2 += '<td class="text-center">' + (item.l == null ? '0' : item.l) + '</td>';
                        hmtlv2 += '<td class="text-center">' + (item.m == null ? '0' : item.m) + '</td>';
                        hmtlv2 += '<td class="text-center">' + (item.x == null ? (parseInt(item.j == null ? 0 : item.j) + parseInt(item.j == null ? 0 / 2 : item.j / 2)) : item.x) + '</td>';
                        hmtlv2 += '<td class="text-center">' + (item.j == null ? '0' : item.j) + '</td>';
                        hmtlv2 += '<td class="text-center">' + (item.v == null ? '0' : item.v) + '</td>';
                        hmtlv2 += '<td class="text-center">' + (item.s == null ? '0' : item.s) + '</td>';
                        hmtlv2 += '</tr>';
                    });
                    hmtlv2 += '<tr>';
                    hmtlv2 += '<td class="text-center">Total Promedio</td>';
                    hmtlv2 += '<td class="text-center">' + Math.round(totald / 24) + '</td>';
                    hmtlv2 += '<td class="text-center">' + Math.round(totall / 24) + '</td>';
                    hmtlv2 += '<td class="text-center">' + Math.round(totalm / 24) + '</td>';
                    hmtlv2 += '<td class="text-center">' + Math.round(totalx / 24) + '</td>';
                    hmtlv2 += '<td class="text-center">' + Math.round(totalj / 24) + '</td>';
                    hmtlv2 += '<td class="text-center">' + Math.round(totalv / 24) + '</td>';
                    hmtlv2 += '<td class="text-center">' + Math.round(totals / 24) + '</td>';
                    hmtlv2 += '</tr>';
                    // var popup = L.popup({
                    //     closeButton: true,
                    //     minWidth: 450
                    // }).setLatLng(latlng).setContent(
                    //     '<div class="text-center"><h5><strong>Data desde el 01/12/2018 al 07/12/2018 </strong></h5></div><br><table border="0" class="table table-sm table-mariconada"><tr><td class="text-center bold">Hora</td><td class="text-center bold">Dom</td><td class="text-center bold">Lun</td><td class="text-center bold">Mar</td><td class="text-center bold">Mie</td><td class="text-center bold">Jue</td><td class="text-center bold">Vie</td><td class="text-center bold">Sab</td></tr>' + hmtl + '</table><div><strong>*La consulta se realizó con la coordenada central:</strong> (' + lat.toString().slice(0, 10) + '; ' + lng.toString().slice(0, 10) + ') <br><strong>y un radio de: </strong>' + Math.round(radio) + ' metros </div>'
                    // ).openOn(map);



                    // $('#map').css('cursor', 'pointer');
                });
                // }
                console.log(hmtlv2);
                $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params_estado)), function(response_estado) {
                    if (response_estado.length > 0) {
                        $.each(response_estado, function(i, item) {
                            hmtl_estado += '<tr>';
                            hmtl_estado += '<td>' + item.clasifica + '</td>';
                            hmtl_estado += '<td class="text-right">' + item.cant + '</td>';
                            hmtl_estado += '</tr>';
                        });
                    }
                });
                $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params)), function(response) {
                    if (response.length > 0) {
                        $.each(response, function(i, item) {
                            hmtl += '<tr>';
                            hmtl += '<td>' + item.generico + '</td>';
                            hmtl += '<td class="text-right">' + item.cant + '</td>';
                            hmtl += '</tr>';
                        });
                        var popup = L.popup({
                            closeButton: true,
                            minWidth: 900
                        }).setLatLng(latlng).setContent(
                            '<div class="row"><div class="col-lg-6"><div id="analisis_det_chartv2" style="min-height: 400px;display: flex; align-items: center; justify-content: center;"><img src="img/200.gif"/></div><br ><div><table border="0" class="table table-sm table-mariconada"><tr><td class="text-center bold">MODALIDAD DE DELITO</td><td class="text-center bold">CANTIDAD</td></tr>' + hmtl + '</table><table border="0" class="table table-sm table-mariconada"><tr><td class="text-center bold">GEOCERCAS</td><td class="text-center bold">CANTIDAD</td></tr>' + hmtl_estado + '</table></div><div><strong>*La consulta se realizó con la coordenada central:</strong> (' + lat.toString().slice(0, 10) + '; ' + lng.toString().slice(0, 10) + ') <br><strong>y un radio de: </strong>' + Math.round(radio) + ' metros </div></div>   <div class="col-lg-6"> <div class="text-center"><h5><strong>Data desde el 01/12/2018 al 07/12/2018 </strong></h5></div><br><table border="0" class="table table-sm table-mariconada"><tr><td class="text-center bold">Hora</td><td class="text-center bold">Dom</td><td class="text-center bold">Lun</td><td class="text-center bold">Mar</td><td class="text-center bold">Mie</td><td class="text-center bold">Jue</td><td class="text-center bold">Vie</td><td class="text-center bold">Sab</td></tr>' + hmtlv2 + '</table><div><strong>*La consulta se realizó con la coordenada central:</strong> (' + lat.toString().slice(0, 10) + '; ' + lng.toString().slice(0, 10) + ') <br><strong>y un radio de: </strong>' + Math.round(radio) + ' metros </div>   </div></div>'
                        ).openOn(map);
                        fs_grafico({
                            method: 'fs_analisis_det_chart_v2',
                            grafico: 'analisis_det_chartv2',
                            lanlng: latlng,
                            table: baselayer,
                            radio: radio
                        });
                    }
                    $('#map').css('cursor', 'pointer');
                });
            }
        }

        function hexagonos_det(layerid, latlng) {
            var hmtl = '';
            var parent = baseLayers.filter(p => p.key == 'ghexagonos').shift().layers.filter(p => p.id == layerid).shift().parent;
            var baselayer = overLayers.filter(p => p.group == 'mask_hexagonos').shift().layers.filter(p => p.key == parent).shift().table;
            var params = {
                method: 'fs_analisis_det',
                lanlng: latlng,
                table: 'tmp_20201020_1'
            };
            if (baselayer.length > 0) {
                /*const chart = await fetch('php/app.php?data=' + encodeURIComponent(JSON.stringify({...params, method: 'fs_analisis_det_chart'})));				
                console.log(await chart.json());*/

                $('#map').css('cursor', 'progress');

                $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params)), function(response) {
                    if (response.length > 0) {
                        $.each(response, function(i, item) {
                            hmtl += '<tr>';
                            hmtl += '<td>' + item.generico + '</td>';
                            hmtl += '<td class="text-center">' + item.a2016 + '</td>';
                            hmtl += '<td class="text-center">' + item.a2017 + '</td>';
                            hmtl += '<td class="text-center">' + item.a2018 + '</td>';
                            hmtl += '<td class="text-center">' + item.a2019 + '</td>';
                            hmtl += '<td class="text-center">' + item.a2020 + '</td>';
                            hmtl += '<td class="text-center">' + item.a2021 + '</td>';
                            hmtl += '<td class="text-center">' + item.cant + '</td>';
                            hmtl += '</tr>';
                        });

                        hmtl += '<tr class="table-total">';
                        hmtl += '<td class="text-center bold">Total</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2016
                        }) => acc + Number(a2016), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2017
                        }) => acc + Number(a2017), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2018
                        }) => acc + Number(a2018), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2019
                        }) => acc + Number(a2019), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2020
                        }) => acc + Number(a2020), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            a2021
                        }) => acc + Number(a2021), 0) + '</td>';
                        hmtl += '<td class="text-center bold">' + response.reduce((acc, {
                            cant
                        }) => acc + Number(cant), 0) + '</td>';
                        hmtl += '</tr>';

                        var popup = L.popup({
                            closeButton: true,
                            minWidth: 900
                        }).setLatLng(latlng).setContent(
                            `<div id="analisis_det_chart" style="min-height: 400px;display: flex; align-items: center; justify-content: center;">
                                <img src="img/200.gif"/>
                            </div>
                            <table border="0" class="table table-sm table-mariconada">
                                <tr>
                                    <td class="text-center bold">MODALIDAD DE DELITO</td>
                                    <td class="text-center bold">2016</td>
                                    <td class="text-center bold">2017</td>
                                    <td class="text-center bold">2018</td>
                                    <td class="text-center bold">2019</td>
                                    <td class="text-center bold">2020</td>
                                    <td class="text-center bold">2021</td>
                                    <td class="text-center bold">CANTIDAD</td>
                                </tr>` + hmtl + `</table>`
                        ).openOn(map);

                        fs_grafico({
                            method: 'fs_analisis_det_chart',
                            grafico: 'analisis_det_chart',
                            lanlng: latlng,
                            table: baselayer
                        });
                    }

                    $('#map').css('cursor', 'pointer');
                });
            }
        }

        /**********inicio*/
        function hexagonos_det_pnp_sereno(layerid, latlng) {
            var hmtl = '';
            var hmtl2 = '';
            var hex = '';
            // var parent = baseLayers.filter(p => p.key == 'ghexagonos').shift().layers.filter(p => p.id == layerid).shift().parent;
            var overLayer = overLayers.filter(p => p.key == 'delitos_serenos').shift().layers.filter(p => p.id == layerid)[0].key;

            var params_1 = {
                method: 'fs_analisis_det_pnp2',
                lanlng: latlng,
                table: 'delitos_serenos',
                where: 'no_preventivo_delito'
            };
            var params_2 = {
                method: 'fs_analisis_det_pnp2',
                lanlng: latlng,
                table: 'delitos_serenos',
                where: 'no_preventivo_falta'
            };
            var params_3 = {
                method: 'fs_analisis_det_pnp2',
                lanlng: latlng,
                table: 'delitos_serenos',
                where: 'preventivo_generico'
            };
            switch (overLayer) {
                case 'hex_pnp':
                    console.log("pnp_sereno");
                    if (overLayers.length > 0) {
                        $('#map').css('cursor', 'progress');
                        $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params_1)), function(response_pnp) {
                            var total = 0;
                            if (response_pnp.length > 0) {
                                $.each(response_pnp, function(i, item) {
                                    total = total + parseInt(item.cant);
                                    hex = item.hexagono;
                                    hmtl2 += '<tr>';
                                    hmtl2 += '<td>' + item.nivel_1 + '</td>';
                                    hmtl2 += '<td class="text-right">' + item.cant + '</td>';
                                    hmtl2 += '</tr>';

                                });
                                hmtl2 += '</tr>';
                                hmtl2 += '<td>' + 'Total' + '</td>';
                                hmtl2 += '<td class="text-right">' + total + '</td>';
                                hmtl2 += '</tr>';
                                var popup = L.popup({
                                    closeButton: true,
                                    minWidth: 500
                                }).setLatLng(latlng).setContent(
                                    `<p><center><h4><b style="font-weight: 500;font-family: sans-serif;">Delitos No preventivos(PNP) - Geocerca #` + hex + `</b></h4></center></p>
                                        <br><table border="0" class="table table-sm table-mariconada">
                                            <tr>
                                            <td class="text-center bold">MODALIDAD DE DELITO</td><td class="text-center bold">CANTIDAD</td></tr>` + hmtl2 + `</table>`
                                ).openOn(map);

                            }
                        });
                    }

                    break;
                case 'hex_no_preventivos':
                    console.log("pnp_sereno");
                    if (overLayers.length > 0) {
                        $('#map').css('cursor', 'progress');
                        $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params_2)), function(response_pnp) {
                            var total = 0;
                            if (response_pnp.length > 0) {
                                $.each(response_pnp, function(i, item) {
                                    total = total + parseInt(item.cant);
                                    hex = item.hexagono;
                                    hmtl2 += '<tr>';
                                    hmtl2 += '<td>' + item.nivel_1 + '</td>';
                                    hmtl2 += '<td class="text-right">' + item.cant + '</td>';
                                    hmtl2 += '</tr>';

                                });
                                hmtl2 += '</tr>';
                                hmtl2 += '<td>' + 'Total' + '</td>';
                                hmtl2 += '<td class="text-right">' + total + '</td>';
                                hmtl2 += '</tr>';
                                var popup = L.popup({
                                    closeButton: true,
                                    minWidth: 500
                                }).setLatLng(latlng).setContent(
                                    `<p><center><h4><b style="font-weight: 500;font-family: sans-serif;">Faltas No Preventivos(Sereno) - Geogeocerca #` + hex + `</b></h4></center></p>
                                        <br><table border="0" class="table table-sm table-mariconada">
                                            <tr>
                                            <td class="text-center bold">MODALIDAD DE FALTA </td><td class="text-center bold">CANTIDAD</td></tr>` + hmtl2 + `</table>`
                                ).openOn(map);

                            }
                        });
                    }

                    break;
                case 'hex_preventivos':
                    console.log("pnp_sereno");
                    if (overLayers.length > 0) {
                        $('#map').css('cursor', 'progress');
                        $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params_3)), function(response_pnp) {
                            var total = 0;
                            if (response_pnp.length > 0) {
                                $.each(response_pnp, function(i, item) {
                                    total = total + parseInt(item.cant);
                                    hex = item.hexagono;
                                    hmtl2 += '<tr>';
                                    hmtl2 += '<td>' + item.nivel_1 + '</td>';
                                    hmtl2 += '<td class="text-right">' + item.cant + '</td>';
                                    hmtl2 += '</tr>';

                                });
                                hmtl2 += '</tr>';
                                hmtl2 += '<td>' + 'Total' + '</td>';
                                hmtl2 += '<td class="text-right">' + total + '</td>';
                                hmtl2 += '</tr>';
                                var popup = L.popup({
                                    closeButton: true,
                                    minWidth: 500
                                }).setLatLng(latlng).setContent(
                                    `<p><center><h4><b style="font-weight: 500;font-family: sans-serif;">Faltas Preventivos(Sereno) - Geogeocerca #` + hex + `</b></h4></center></p>
                                        <br><table border="0" class="table table-sm table-mariconada">
                                            <tr>
                                            <td class="text-center bold">MODALIDAD DE FALTA</td><td class="text-center bold">CANTIDAD</td></tr>` + hmtl2 + `</table>`
                                ).openOn(map);


                            }
                        });
                    }

                    break;

            }
        }

        function hexagonos_det_pnp_sereno_radio(layerid, latlng, radio) {
            var hmtl = '';
            var hmtl2 = '';
            var hex = '';
            // var parent = baseLayers.filter(p => p.key == 'ghexagonos').shift().layers.filter(p => p.id == layerid).shift().parent;
            var overLayer = overLayers.filter(p => p.key == 'delitos_serenos').shift().layers.filter(p => p.id == layerid)[0].key;

            var params_1 = {
                method: 'fs_analisis_det_pnp',
                lanlng: latlng,
                table: 'delitos_serenos',
                where: 'no_preventivo_delito',
                radio: radio
            };
            var params_2 = {
                method: 'fs_analisis_det_pnp',
                lanlng: latlng,
                table: 'delitos_serenos',
                where: 'no_preventivo_falta',
                radio: radio
            };
            var params_3 = {
                method: 'fs_analisis_det_pnp',
                lanlng: latlng,
                table: 'delitos_serenos',
                where: 'preventivo_generico',
                radio: radio
            };
            console.log(params_1)
            switch (overLayer) {
                case 'hex_pnp':
                    console.log("pnp_sereno");
                    if (overLayers.length > 0) {
                        $('#map').css('cursor', 'progress');
                        $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params_1)), function(response_pnp) {
                            var total = 0;
                            if (response_pnp.length > 0) {
                                $.each(response_pnp, function(i, item) {
                                    total = total + parseInt(item.cant);
                                    hmtl2 += '<tr>';
                                    hmtl2 += '<td>' + item.nivel_1 + '</td>';
                                    hmtl2 += '<td class="text-right">' + item.cant + '</td>';
                                    hmtl2 += '</tr>';

                                });
                                hmtl2 += '</tr>';
                                hmtl2 += '<td>' + 'Total' + '</td>';
                                hmtl2 += '<td class="text-right">' + total + '</td>';
                                hmtl2 += '</tr>';
                                var popup = L.popup({
                                    closeButton: true,
                                    minWidth: 350
                                }).setLatLng(latlng).setContent(
                                    `<p><center><h4><b style="font-weight: 500;font-family: sans-serif;">Delitos No preventivos(PNP) </b></h4></center></p>
                                        <br><table border="0" class="table table-sm table-mariconada">
                                            <tr>
                                            <td class="text-center bold">MODALIDAD DE DELITO</td><td class="text-center bold">CANTIDAD</td></tr>` + hmtl2 + `</table>`
                                ).openOn(map);

                            }
                        });
                    }

                    break;
                case 'hex_no_preventivos':
                    console.log("pnp_sereno");
                    if (overLayers.length > 0) {
                        $('#map').css('cursor', 'progress');
                        $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params_2)), function(response_pnp) {
                            var total = 0;
                            if (response_pnp.length > 0) {
                                $.each(response_pnp, function(i, item) {
                                    total = total + parseInt(item.cant);
                                    hmtl2 += '<tr>';
                                    hmtl2 += '<td>' + item.nivel_1 + '</td>';
                                    hmtl2 += '<td class="text-right">' + item.cant + '</td>';
                                    hmtl2 += '</tr>';

                                });
                                hmtl2 += '</tr>';
                                hmtl2 += '<td>' + 'Total' + '</td>';
                                hmtl2 += '<td class="text-right">' + total + '</td>';
                                hmtl2 += '</tr>';
                                var popup = L.popup({
                                    closeButton: true,
                                    minWidth: 350
                                }).setLatLng(latlng).setContent(
                                    `<p><center><h4><b style="font-weight: 500;font-family: sans-serif;">Faltas No Preventivos(Sereno) </b></h4></center></p>
                                        <br><table border="0" class="table table-sm table-mariconada">
                                            <tr>
                                            <td class="text-center bold">MODALIDAD DE FALTA </td><td class="text-center bold">CANTIDAD</td></tr>` + hmtl2 + `</table>`
                                ).openOn(map);

                            }
                        });
                    }

                    break;
                case 'hex_preventivos':
                    console.log("pnp_sereno");
                    if (overLayers.length > 0) {
                        $('#map').css('cursor', 'progress');
                        $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params_3)), function(response_pnp) {
                            var total = 0;
                            if (response_pnp.length > 0) {
                                $.each(response_pnp, function(i, item) {
                                    total = total + parseInt(item.cant);
                                    hmtl2 += '<tr>';
                                    hmtl2 += '<td>' + item.nivel_1 + '</td>';
                                    hmtl2 += '<td class="text-right">' + item.cant + '</td>';
                                    hmtl2 += '</tr>';

                                });
                                hmtl2 += '</tr>';
                                hmtl2 += '<td>' + 'Total' + '</td>';
                                hmtl2 += '<td class="text-right">' + total + '</td>';
                                hmtl2 += '</tr>';
                                var popup = L.popup({
                                    closeButton: true,
                                    minWidth: 350
                                }).setLatLng(latlng).setContent(
                                    `<p><center><h4><b style="font-weight: 500;font-family: sans-serif;">Faltas Preventivos(Sereno) </b></h4></center></p>
                                        <br><table border="0" class="table table-sm table-mariconada">
                                            <tr>
                                            <td class="text-center bold">MODALIDAD DE FALTA</td><td class="text-center bold">CANTIDAD</td></tr>` + hmtl2 + `</table>`
                                ).openOn(map);


                            }
                        });
                    }

                    break;

            }
        }
        /**********fin */
        function mapa_calor(latlng) {
            var layerid = $('.leaflet-panel-layers-group input[type="radio"]:checked');

            if (layerid.length == 0) {
                return;
            }

            $('#map').css('cursor', 'progress');

            $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify({
                method: 'fs_nom_dist',
                lanlng: latlng
            })), function(response) {
                if (response.length > 0) {
                    var distrito = response.shift();

                    if (lyr_mapa_calor_idx.includes(distrito.id)) {
                        $('#map').css('cursor', 'pointer');
                        return;
                    }

                    lyr_mapa_calor_idx.push(distrito.id);

                    var popup = L.popup({
                        closeButton: true,
                        minWidth: 130
                    }).setLatLng(latlng).setContent('<img src="img/35.gif"/><b> Descargando mapa de calor de ' + distrito.nombre + '</b>').openOn(map);
                    mapa_calor_distrito(distrito.id, layerid.val(), popup)
                }
            });
        }

        function mapa_calor_distrito(iddist, layerid, popup) {
            var tmp = [];
            var parent = baseLayers.filter(p => p.key == 'ghexagonos').shift().layers.filter(p => p.id == layerid).shift().parent;
            var baselayer = overLayers.filter(p => p.group == 'mask_hexagonos').shift().layers.filter(p => p.key == parent).shift().table;

            $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify({
                method: 'fs_mapa_calor',
                iddist: iddist,
                table: baselayer
            })), function(response) {
                if (response.length > 0) {
                    $.each(response, function(i, item) {
                        tmp.push([Number(item.latitud), Number(item.longitud), 0.15])
                    });

                    L.heatLayer(tmp, {
                        minOpacity: 0.5
                    }).addTo(lyr_mapa_calor);
                }

                if (map.hasLayer(popup)) {
                    map.removeLayer(popup);
                }

                $('#map').css('cursor', 'pointer');
            });
        }

        function lupa() {
            map.pm.addControls({
                drawMarker: false,
                drawPolygon: false,
                editPolygon: true,
                cutPolygon: false,
                drawPolyline: false,
                drawCircle: true,
                drawCircleMarker: false,
                drawRectangle: false,
                deleteLayer: true,
                customControls: true,
                optionsControls: true,
                oneBlock: false,

            });
            map.pm.setGlobalOptions({
                maxRadiusCircle: 1250,
                editMode: true,
                dragMode: true
            });

            // map.on('pm:buttonclick',function(e){
            //     flagCircle=2;
            // })
            map.on('pm:create', function(e) {

                var layer = e.layer;
                var fg = L.featureGroup();
                fg.addLayer(layer);
                var theCenterPt = layer.getLatLng();
                var theRadius = layer.getRadius();
                var layerid = $('.leaflet-panel-layers-group input[type="radio"]:checked');

                var layerid_todos = $('.leaflet-panel-layers-group input[type="checkbox"]:checked').val();
                console.log("layer_todo", layerid_todos);
                console.log("layer_val", layerid.val());

                var layer = $('.leaflet-panel-layers-group input[type="checkbox"]:checked');
                /**inicio */

                if ($('#chkHexagonos').is(':checked')) {
                    hexagono_det_(layerid.val(), theCenterPt, theRadius);
                }
                if (flagCircle == 1 && (map.hasLayer(overLayers.filter(p => p.key == 'delitos_serenos')[0].layers.filter(p => p.key == 'hex_pnp').shift().layer) || map.hasLayer(overLayers.filter(p => p.key == 'delitos_serenos')[0].layers.filter(p => p.key == 'hex_preventivos').shift().layer) || map.hasLayer(overLayers.filter(p => p.key == 'delitos_serenos')[0].layers.filter(p => p.key == 'hex_no_preventivos').shift().layer)) && !$("#chkHexagonos").is(':checked')) {
                    hexagonos_det_pnp_sereno_radio(layer.val(), theCenterPt, theRadius);
                }

                if (map.hasLayer(overLayers.filter(p => p.key == 'delitosaqp').shift().layer)) {
                    //aqi va cuanod contruya el doble ventana
                    console.log("asdasda");
                    hexagono_det_aqp_radio(theCenterPt, theRadius);
                }

                if (map.hasLayer(overLayers.filter(p => p.key == 'densidad').shift().layer) && !($('#chkHexagonos').is(':checked'))) {
                    //aqi va cuanod contruya el doble ventana
                    hexagono_det_densidad('data_lima_x_punto', 'hex_lima_punto', theCenterPt, theRadius);
                } else if (map.hasLayer(overLayers.filter(p => p.key == 'densidad').shift().layer) && $('#chkHexagonos').is(':checked')) {
                    hexagono_det_doble(layerid.val(), theCenterPt, theRadius);
                }

                if (map.hasLayer(overLayers.filter(p => p.key == 'delitosLal').shift().layer) && !($('#chkHexagonos').is(':checked'))) {
                    //aqi va cuanod contruya el doble ventana
                    hexagono_det_lal_radio('no_va_nada', theCenterPt, theRadius);
                }

                if (map.hasLayer(overLayers.filter(p => p.key == 'delitosAlto').shift().layer) && !($('#chkHexagonos').is(':checked'))) {
                    //aqi va cuanod contruya el doble ventana
                    hexagono_det_elalto_radio('no_va_nada', theCenterPt, theRadius);
                }

                if (map.hasLayer(overLayers.filter(p => p.key == 'delitosaqp').shift().layer) && !($('#chkHexagonos').is(':checked'))) {
                    //aqi va cuanod contruya el doble ventana
                    hexagono_det_aqp_radio('no_va_nada', theCenterPt, theRadius);
                }

                if (map.hasLayer(overLayers.filter(p => p.key == 'densidadaqp').shift().layer) && !($('#chkHexagonos').is(':checked'))) {
                    //aqi va cuanod contruya el doble ventana
                    console.log('eeeeee');
                    hexagono_det_densidad('data_aqp_x_punto', 'hex_aqp_punto', theCenterPt, theRadius);
                }
                // else if (map.hasLayer(overLayers.filter(p => p.key == 'densidadaqp').shift().layer) && $('#chkHexagonos').is(':checked')) {

                //     hexagono_det_doble(layerid.val(), theCenterPt, theRadius);
                // }

                if (layerid_todos != undefined) {
                    if (densidad.includes(layerid_todos.toString())) {
                        console.log("ent_crear");
                        hexagono_det_densidad('data_lima_x_punto', 'hex_lima_punto', theCenterPt, theRadius);
                    }
                }
                // if (layerid_todos != undefined) {
                //     if (densidad.includes(layerid_todos.toString())) {
                //         console.log("ent_crear");
                //         hexagono_det_densidad('data_aqp_x_punto', 'hex_aqp_punto', theCenterPt, theRadius);
                //     }
                // }

                // flagCircle = 1;
                // layer.on('pm:edit', function(e) {
                //     console.log("editado");
                //     var layer = e.layer;
                //     var fg = L.featureGroup();
                //     fg.addLayer(layer);
                //     var theCenterPt = layer.getLatLng();
                //     var theRadius = layer.getRadius();
                //     // console.log(theCenterPt, ' ', theRadius);
                //     var layerid = $('.leaflet-panel-layers-group input[type="radio"]:checked');
                //     var layerid_todos = $('.leaflet-panel-layers-group input[type="checkbox"]:checked').val();
                //     console.log(layerid_todos);
                //     console.log(layerid.val());


                //     if ($('#chkHexagonos').is(':checked')) {
                //         hexagono_det_(layerid.val(), theCenterPt, theRadius);
                //     }
                //     if (map.hasLayer(overLayers.filter(p => p.key == 'delitosaqp').shift().layer)) {
                //         //aqi va cuanod contruya el doble ventana
                //         console.log("asdasda");
                //         hexagono_det_aqp(theCenterPt, theRadius);


                //     }
                //     if (map.hasLayer(overLayers.filter(p => p.key == 'densidad').shift().layer) && !($('#chkHexagonos').is(':checked'))) {
                //         //aqi va cuanod contruya el doble ventana
                //         hexagono_det_densidad('data_lima_x_punto', 'hex_lima_punto', theCenterPt, theRadius);
                //     } else if (map.hasLayer(overLayers.filter(p => p.key == 'densidad').shift().layer) && $('#chkHexagonos').is(':checked')) {

                //         hexagono_det_doble(layerid.val(), theCenterPt, theRadius);
                //     }

                //     if (map.hasLayer(overLayers.filter(p => p.key == 'densidadaqp').shift().layer) && !($('#chkHexagonos').is(':checked'))) {
                //         //aqi va cuanod contruya el doble ventana
                //         hexagono_det_densidad('data_aqp_x_punto', 'hex_aqp_punto', theCenterPt, theRadius);
                //     } else if (map.hasLayer(overLayers.filter(p => p.key == 'densidadaqp').shift().layer) && $('#chkHexagonos').is(':checked')) {

                //         hexagono_det_doble(layerid.val(), theCenterPt, theRadius);
                //     }

                //     if (layerid_todos != undefined) {
                //         if (densidad.includes(layerid_todos.toString())) {
                //             console.log("ent_crear");
                //             hexagono_det_densidad('data_lima_x_punto', 'hex_lima_punto', theCenterPt, theRadius);
                //         }
                //     }
                //     // if (layerid_todos != undefined) {
                //     //     if (densidad.includes(layerid_todos.toString())) {
                //     //         console.log("ent_crear");
                //     //         hexagono_det_densidad('data_aqp_x_punto', 'hex_aqp_punto', theCenterPt, theRadius);
                //     //     }
                //     // }

                // });



            });
        }

        function logo() {
            L.Control.Watermark = L.Control.extend({
                onAdd: function(map) {
                    var img = L.DomUtil.create('img');

                    img.id = 'logo';
                    img.src = 'http://sensor.colaboraccion.pe/assets/app/img/logo.png';
                    img.style.width = '220px';

                    return img;
                },
                onRemove: function(map) {}
            });

            L.control.watermark = function(opts) {
                return new L.Control.Watermark(opts);
            }

            L.control.watermark({
                position: 'topleft'
            }).addTo(map);
        }

        function search() {
            L.control.scale().addTo(map);
            var searchControl = new L.esri.Controls.Geosearch().addTo(map);
        }

        function legend() {
            var legend = L.control({
                position: 'bottomleft'
            });

            legend.onAdd = function(map) {

                var div = L.DomUtil.create('div', 'info legend');
                var str = '<p><b>Leyenda de Hexágonos</b></p>';
                str += '<table id="legend" class="table" width="100%">';
                str += '<tr>';
                str += '<td style="background:#8C0000;opacity: 0.66;"></td>';
                str += '<td>Muy alto</td>';
                str += '</tr>';
                str += '<tr>';
                str += '<td style="background:#D90000;opacity: 0.66;"></td>';
                str += '<td>Alto</td>';
                str += '</tr>';
                str += '<tr>';
                str += '<td style="background:#FF5C26;opacity: 0.66;"></td>';
                str += '<td>Medio</td>';
                str += '</tr>';
                str += '<tr>';
                str += '<td style="background:#d2b113;opacity: 0.66;"></td>';
                str += '<td>Bajo</td>';
                str += '</tr>';
                str += '<tr>';
                str += '<td style="background:#FFC926;opacity: 0.66;" width="15%"></td>';
                str += '<td>Muy bajo</td>';
                str += '</tr>';
                str += '</table>';

                div.innerHTML = str;

                return div;
            };

            legend.addTo(map);
        }

        function sidebar() {
            sidebar = L.control.sidebar('sidebar', {
                closeButton: true,
                position: 'left',
                autoPan: false
            });

            map.addControl(sidebar);

            sidebar2 = L.control.sidebar('sidebar2', {
                closeButton: true,
                position: 'right',
                autoPan: false
            });

            map.addControl(sidebar2);

            $('a.close').appendTo("#sidebar > .panel > .panel-heading, #sidebar2 > .panel > .panel-heading");
            $('#sidebar2 .close').click(function() {
                sidebar2.toggle();
            });

            $('[data-toggle="collapse"]').on('click', function(e) {
                e.preventDefault();

                var id = $(this).attr('href');

                if ($(id).hasClass('in')) {
                    $(id).collapse('hide');
                } else {
                    $(id).collapse('show');
                }
            });
        }

        function buttons() {
            L.easyButton({
                id: 'btnMain',
                position: 'topleft',
                type: 'replace',
                leafletClasses: true,
                states: [{
                    stateName: 'get-center',
                    onClick: function(button, map) {
                        sidebar.toggle();
                    },
                    title: 'Filtros',
                    icon: 'glyphicon-menu-hamburger'
                }]
            }).addTo(map);

            // L.easyButton({
            //     id: 'btndraw',
            //     position: 'topleft',
            //     type: 'replace',
            //     leafletClasses: true,
            //     states: [{
            //         stateName: 'get-center',
            //         onClick: function (button, map) {
            //             sidebar.toggle();
            //         },
            //         title: 'Filtros',
            //         icon: 'glyphicon-menu-hamburger'
            //     }]
            // }).addTo(map);

            L.easyButton({
                id: 'btnChart',
                position: 'topleft',
                type: 'replace',
                leafletClasses: true,
                states: [{
                    stateName: 'get-center',
                    onClick: function(button, map) {
                        sidebar2.toggle();
                    },
                    title: 'Gráficos',
                    icon: 'glyphicon glyphicon-signal'
                }]
            }).addTo(map);

            L.easyButton({
                id: 'btnChart',
                position: 'topleft',
                type: 'replace',
                leafletClasses: true,
                states: [{
                    stateName: 'get-center',
                    onClick: function(button, map) {
                        $('#myModal').modal('show');
                    },
                    title: 'Análisis',
                    icon: 'glyphicon glyphicon-list-alt'
                }]
            }).addTo(map);

            L.easyButton({
                id: 'btnHome',
                position: 'topleft',
                type: 'replace',
                leafletClasses: true,
                states: [{
                    stateName: 'get-center',
                    onClick: function(button, map) {
                        map.setView([-12.099298, -77.030706], 15);
                    },
                    title: 'Inicio',
                    icon: 'glyphicon-home'
                }]
            }).addTo(map);

            L.easyButton({
                id: 'btnLayers',
                position: 'topleft',
                type: 'replace',
                leafletClasses: true,
                states: [{
                    stateName: 'get-center',
                    title: 'Usuario: ',
                    icon: 'glyphicon glyphicon-user'
                }]
            }).addTo(map);

            L.easyButton({
                id: 'btnLayers',
                position: 'topleft',
                type: 'replace',
                leafletClasses: true,
                states: [{
                    stateName: 'get-center',
                    onClick: function(button, map) {
                        window.location.href = "session.php?cerrar";
                    },
                    title: 'Cerrar sesión',
                    icon: 'glyphicon glyphicon-off'
                }]
            }).addTo(map);

            /*L.easyButton({
                id: 'btnLayers',
                position: 'topleft',
                type: 'replace',
                leafletClasses: true,
                states: [{
                    stateName: 'get-center',
                    onClick: function (button, map) {
                        map.setZoom(map.getZoom() + 1);
                    },
                    title: 'Acercar',
                    icon: 'glyphicon glyphicon-plus'
                }]
            }).addTo(map);

            L.easyButton({
                id: 'btnLayers',
                position: 'topleft',
                type: 'replace',
                leafletClasses: true,
                states: [{
                    stateName: 'get-center',
                    onClick: function (button, map) {
                        map.setZoom(map.getZoom() - 1);
                    },
                    title: 'Alejar',
                    icon: 'glyphicon glyphicon-minus'
                }]
            }).addTo(map);*/
        }

        function panel_layers() {
            baseLayers = [{
                key: "ghexagonos",
                group: "Años",
                collapsed: false,
                layers: [{
                        key: "mask_hexagonos",
                        parent: "hexagonos",
                        name: "&nbsp;Todos",
                        layer: (function() {
                            var l = L.geoJson();
                            return l;
                        }())
                    },
                    {
                        key: "mask_hexagonos2016",
                        parent: "hexagonos2016",
                        name: "&nbsp;2016",
                        layer: (function() {
                            var l = L.geoJson();
                            return l;
                        }())
                    },
                    {
                        key: "mask_hexagonos2017",
                        parent: "hexagonos2017",
                        name: "&nbsp;2017",
                        layer: (function() {
                            var l = L.geoJson();
                            return l;
                        }())
                    },
                    {
                        key: "mask_hexagonos2018",
                        parent: "hexagonos2018",
                        name: "&nbsp;2018",
                        layer: (function() {
                            var l = L.geoJson();
                            return l;
                        }())
                    },
                    {
                        key: "mask_hexagonos2019",
                        parent: "hexagonos2019",
                        name: "&nbsp;2019",
                        layer: (function() {
                            var l = L.geoJson();
                            return l;
                        }())
                    },
                    {
                        key: "mask_hexagonos2020",
                        parent: "hexagonos2020",
                        name: "&nbsp;2020",
                        layer: (function() {
                            var l = L.geoJson();
                            return l;
                        }())
                    },
                    {
                        key: "mask_hexagonos2021",
                        parent: "hexagonos2021",
                        name: "&nbsp;2021",
                        layer: (function() {
                            var l = L.geoJson();
                            return l;
                        }())
                    }
                ]
                /**/
            }];

            var excludeLayers = ['mask_hexagonos', 'mask_hexagonos2016', 'mask_hexagonos2017', 'mask_hexagonos2018', 'mask_hexagonos2019', 'mask_hexagonos2020', 'mask_hexagonos2021', 'hexagonos', 'hexagonos2016', 'hexagonos2017', 'hexagonos2018', 'hexagonos2019', 'hexagonos2020', 'hexagonos2021', 'mapa_calor', 'ubigeos', 'densidad', 'densidadaqp', 'delitosaqp', 'puntos', 'dgenericos', 'faltas', 'modalidad', 'hex_densidad', 'hex_pc1', 'hex_pc2', 'hex_pc3', 'hex_pc4'];

            L.TopoJSON = L.GeoJSON.extend({
                addData: function(jsonData) {
                    if (jsonData.type === "Topology") {
                        for (key in jsonData.objects) {
                            geojson = topojson.feature(jsonData, jsonData.objects[key]);
                            L.GeoJSON.prototype.addData.call(this, geojson);
                        }
                    } else {
                        L.GeoJSON.prototype.addData.call(this, jsonData);
                    }
                }
            });

            overLayers = [
                //deltios
                {
                    key: "delitos_serenos",
                    collapsed: false,
                    group: "Delitos",
                    layers: [{
                            key: "todos",
                            active: false,
                            name: "&nbsp;Todos",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:delitos_serenos',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6
                                }]
                            }
                        },
                        {
                            key: "nopreventivos",
                            active: false,
                            name: "&nbsp;Serenazgo - PNP",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:delitos_serenos',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6,
                                    cql_filter: "nivel in(1,2)",

                                }]
                            }
                        },
                        {
                            key: "preventivos",
                            active: false,
                            name: "&nbsp;Serenazgo",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:delitos_serenos',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6,
                                    cql_filter: "nivel = 3",

                                }]
                            }
                        },
                        {
                            key: "hex_pnp",
                            active: false,
                            name: "&nbsp;<img class='delitoPNP' src='./assets/img/hexRojo.jpg' style='width:12px;heigth:12px'>&nbsp;Delitos PNP",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:hex_pnp',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6,
                                }]
                            }
                        },
                        {
                            key: "hex_no_preventivos",
                            active: false,
                            name: "&nbsp;<img class='delitoPNP' src='./assets/img/hexRojo.jpg' style='width:12px;heigth:12px'>&nbsp;S. No Preventivo",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:hex_pnp_sereno_no_preventivo',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6,
                                }]
                            }
                        },
                        {
                            key: "hex_preventivos",
                            active: false,
                            name: "&nbsp;<img class='delitoPNP' src='./assets/img/hexVerde.png' style='width:11px;heigth:11px'>&nbsp;S. Preventivo",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:hex_pnp_sereno_preventivo',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6,
                                }]
                            }
                        },
                    ]
                },
                // {
                //     key: "delitosaqp",
                //     active: false,
                //     name: "&nbsp; Hex. Delitos AQP",
                //     layer: {
                //         type: "tileLayer.wms",
                //         args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                //             layers: 'colaboraccion:hex_arequipa',
                //             format: 'image/png',
                //             transparent: true,
                //             opacity: 0.6,
                //             cql_filter: "quintil > 0  "
                //         }]
                //     }
                // },
                {
                    key: "delitosaqp",
                    active: false,
                    name: "&nbsp;<img class='delitoPNP' src='./assets/img/hexRojo.jpg' style='width:12px;heigth:12px'>&nbsp;Delitos AQP",
                    layer: {
                        type: "tileLayer.wms",
                        args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                            layers: 'colaboraccion:hex_arequipa_new',
                            format: 'image/png',
                            transparent: true,
                            opacity: 0.6,
                            cql_filter: "quintil > 0"
                        }]
                    }
                },
                {
                    key: "delitosLal",
                    active: false,
                    name: "&nbsp;<img class='delitoPNP' src='./assets/img/hexRojo.jpg' style='width:12px;heigth:12px'>&nbsp;Delitos LAL",
                    layer: {
                        type: "tileLayer.wms",
                        args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                            layers: 'colaboraccion:hex_trujillo',
                            format: 'image/png',
                            transparent: true,
                            opacity: 0.6,
                            cql_filter: "quintil > 0  "
                        }]
                    }
                },
                {
                    key: "delitosAlto",
                    active: false,
                    name: "&nbsp;<img class='delitoPNP' src='./assets/img/hexRojo.jpg' style='width:12px;heigth:12px'>&nbsp;Delitos ElAlto",
                    layer: {
                        type: "tileLayer.wms",
                        args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                            layers: 'colaboraccion:hex_elalto',
                            format: 'image/png',
                            transparent: true,
                            opacity: 0.6,
                            cql_filter: "quintil > 0  "
                        }]
                    }
                },
                //densidad vehiculo
                {
                    key: "densidad",
                    active: false,
                    name: "&nbsp; Densidad Vehiculos",
                    layer: {
                        type: "tileLayer.wms",
                        args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                            layers: 'colaboraccion:densidad_sunat',
                            format: 'image/png',
                            transparent: true,
                            opacity: 0.6
                        }]
                    }
                },
                //densidadaqp
                {
                    key: "densidadaqp",
                    active: false,
                    name: "&nbsp; D. Vehiculos AQP",
                    layer: {
                        type: "tileLayer.wms",
                        args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                            layers: 'colaboraccion:densidad_aqp',
                            format: 'image/png',
                            transparent: true,
                            opacity: 0.6
                        }]
                    }
                },
                //informacion
                {
                    active: false,
                    name: "&nbsp;Información",
                    layer: (function() {
                        var geojsonMarkerOptions = {
                            radius: 6,
                            fillColor: "#ff1010",
                            color: "#000",
                            weight: 1,
                            opacity: 1,
                            fillOpacity: 0.8
                        };

                        var greenIcon = L.icon({
                            iconUrl: 'img/info.png',
                            shadowUrl: 'img/info.png'
                        });

                        var topoLayer = new L.TopoJSON(null, {
                            pointToLayer: function(feature, latlng) {
                                return L.marker(latlng, {
                                    title: feature.properties.distrito,
                                    icon: greenIcon
                                });
                                //return L.circleMarker(latlng, geojsonMarkerOptions);
                            },
                            onEachFeature: function(feature, layer) {
                                layer.bindPopup('Cargando...', {
                                    closeButton: true,
                                    minWidth: 500
                                })
                            }
                        });

                        topoLayer.on('click', function(e) {
                            var obj = e.layer.feature.properties;
                            $.get("ficha.php?iddist=" + obj.iddist, function(response) {
                                e.layer._popup.setContent(response);
                            });
                        });

                        $.getJSON('geojson/distritos_lima_callao.topojson', function(topoData) {
                            topoLayer.addData(topoData);
                        });

                        return topoLayer;
                    }())
                },
                //ubigeos
                {
                    key: "ubigeos",
                    active: false,
                    name: "<i class='glyphicon glyphicon glyphicon-tint ubigeos'></i> Ubigeos",
                    layer: {
                        type: "tileLayer.wms",
                        args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                            layers: 'colaboraccion:distritos',
                            format: 'image/png',
                            transparent: true,
                            opacity: 0.6
                        }]
                    }
                },
                //mapa calor
                {
                    key: "mapa_calor",
                    active: false,
                    name: "&nbsp;Mapa de calor",
                    layer: (function() {
                        return lyr_mapa_calor;
                    }())
                },
                //deltios
                {
                    key: "gpuntos",
                    collapsed: true,
                    group: "Mapa del delito",
                    layers: [{
                            key: "puntos",
                            active: false,
                            name: "&nbsp;Todos",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:delitos',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6
                                }]
                            }
                        },
                        {
                            key: "dgenericos",
                            cql_filter: "nivel_0 = 'genericos'",
                            active: false,
                            name: "<i class='glyphicon glyphicon glyphicon-tint genericos'></i> Delitos",
                            layer: (function() {
                                var l = L.geoJson();
                                return l;
                            }())
                        },
                        {
                            key: "faltas",
                            cql_filter: "nivel_0 = 'faltas'",
                            active: false,
                            name: "<i class='glyphicon glyphicon glyphicon-tint faltas'></i> Faltas",
                            layer: (function() {
                                var l = L.geoJson();
                                console.log("L", l);
                                return l;
                            }())
                        },
                        {
                            key: "modalidad",
                            cql_filter: "nivel_0 = 'delito'",
                            active: false,
                            name: "<i class='glyphicon glyphicon glyphicon-tint delito'></i> Modalidad",
                            layer: (function() {
                                var l = L.geoJson();
                                return l;
                            }())
                        },
                        // {
                        //     key: "aqp",
                        //     active: false,
                        //     name: "&nbsp;Delitos Arequipa",
                        //     layer: {
                        //         type: "tileLayer.wms",
                        //         args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                        //             layers: 'colaboraccion_2020:hex_arequipa',
                        //             format: 'image/png',
                        //             transparent: true,
                        //             opacity: 0.6,
                        //             cql_filter: "quintil > 0  "
                        //         }]
                        //     }
                        // }
                    ]
                },
                //densidad
                {
                    key: "gpuntos_densidad",
                    collapsed: true,
                    group: "P. Control sunat",
                    layers: [{
                            key: "hex_densidad",
                            active: false,
                            name: "&nbsp;Todos",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:densidad_sunat',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6

                                }]
                            }
                        },

                        {
                            key: "hex_pc1",
                            active: false,
                            name: "&nbsp;PC SUNAT Ancón",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:densidad_sunat',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6,
                                    cql_filter: "punto = 'pc1' "
                                }]
                            }
                        },
                        // {
                        //     key: "hex_pc2",
                        //     active: false,
                        //     name: "&nbsp;pc2",
                        //     layer: {
                        //         type: "tileLayer.wms",
                        //         args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                        //             layers: 'colaboraccion_2020:densidad_sunat',
                        //             format: 'image/png',
                        //             transparent: true,
                        //             opacity: 0.6,
                        //             cql_filter: "punto = 'pc2' "
                        //         }]
                        //     }
                        // },
                        {
                            key: "hex_pc3",
                            active: false,
                            name: "&nbsp;PC SUNAT Pucusana",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:densidad_sunat',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6,
                                    cql_filter: "punto = 'pc3' "
                                }]
                            }
                        },
                        // {
                        //     key: "hex_pc4",
                        //     active: false,
                        //     name: "&nbsp;pc4",
                        //     layer: {
                        //         type: "tileLayer.wms",
                        //         args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                        //             layers: 'colaboraccion_2020:densidad_sunat',
                        //             format: 'image/png',
                        //             transparent: true,
                        //             opacity: 0.6,
                        //             cql_filter: "punto = 'pc4' "
                        //         }]
                        //     }
                        // },
                        {
                            key: "hex_pc1_LPR",
                            active: false,
                            name: "&nbsp;PN-1 Entrada Ancón",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:densidad_sunat',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6,
                                    cql_filter: "punto = 'pn1' "
                                }]
                            }
                        },
                        {
                            key: "hex_pc2_LPR",
                            active: false,
                            name: "&nbsp;PN-2 Salida Ancón",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:densidad_sunat',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6,
                                    cql_filter: "punto = 'pn2' "
                                }]
                            }
                        },
                        {
                            key: "hex_pc3_LPR",
                            active: false,
                            name: "&nbsp;PS-0 Entrada referencial",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:densidad_sunat',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6,
                                    cql_filter: "punto = 'ps0' "
                                }]
                            }
                        },
                        {
                            key: "hex_pc3_LPR",
                            active: false,
                            name: "&nbsp;PS-1 Salida Pucusana",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:densidad_sunat',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6,
                                    cql_filter: "punto = 'ps1' "
                                }]
                            }
                        },
                        {
                            key: "hex_st0_LPR",
                            active: false,
                            name: "&nbsp;Mercado Santa Anita",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:densidad_sunat',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6,
                                    cql_filter: "punto = 'st0' "
                                }]
                            }
                        },

                        {
                            key: "hex_pc5_LPR",
                            active: false,
                            name: "&nbsp;Puerto Callao Norte",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:densidad_sunat',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6,
                                    cql_filter: "punto = 'pcn' "
                                }]
                            }
                        },
                        {
                            key: "hex_pc6_LPR",
                            active: false,
                            name: "&nbsp;Puerto Callao Sur",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:densidad_sunat',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6,
                                    cql_filter: "punto = 'pcs' "
                                }]
                            }
                        },

                        {
                            key: "hex_pc7_LPR",
                            active: false,
                            name: "&nbsp;Aeropuerto Oeste",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:densidad_sunat',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6,
                                    cql_filter: "punto = 'ao' "
                                }]
                            }
                        },
                        {
                            key: "hex_pc8_LPR",
                            active: false,
                            name: "&nbsp;Aeropuerto Norte",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:densidad_sunat',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6,
                                    cql_filter: "punto = 'an' "
                                }]
                            }
                        },
                        {
                            key: "hex_pc9_LPR",
                            active: false,
                            name: "&nbsp;Aeropuerto Sur",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:densidad_sunat',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6,
                                    cql_filter: "punto = 'as' "
                                }]
                            }
                        },

                    ]
                },
                //seguridad
                {
                    collapsed: true,
                    group: "Seguridad",
                    layers: [{
                            active: false,
                            name: "Analitica Video",
                            layer: (function() {
                                var geojsonMarkerOptions = {
                                    radius: 6,
                                    fillColor: "#ff1010",
                                    color: "#000",
                                    weight: 1,
                                    opacity: 1,
                                    fillOpacity: 0.8
                                };

                                var greenIcon = L.icon({
                                    iconUrl: 'img/camera.png',
                                    shadowUrl: 'img/camera.png'
                                });

                                var topoLayer = new L.TopoJSON(null, {
                                    pointToLayer: function(feature, latlng) {
                                        return L.marker(latlng, {
                                            icon: greenIcon
                                        });
                                    },
                                    onEachFeature: function(feature, layer, latlng) {
                                        var modal = '';
                                        $.get("show.php", {
                                            data: '',
                                            height: height_map,
                                            width: width_map
                                        }, function(response) {
                                            modal = response;
                                            var popup = L.popup({
                                                closeButton: true,
                                                minWidth: 900
                                            }).setLatLng([-12.095852707001384, -77.0450011214560]).setContent(modal);
                                            layer.bindPopup(popup);
                                        });
                                    }
                                });

                                $.getJSON('geojson/hanwha.topojson', function(topoData) {
                                    topoLayer.addData(topoData);
                                });

                                return topoLayer;
                            }())
                        },
                        {
                            active: false,
                            name: "CCTV El alto",
                            layer: (function() {
                                var geojsonMarkerOptions = {
                                    radius: 6,
                                    fillColor: "#ff1010",
                                    color: "#000",
                                    weight: 1,
                                    opacity: 1,
                                    fillOpacity: 0.8
                                };

                                var greenIcon = L.icon({
                                    iconUrl: 'img/camera.png',
                                    shadowUrl: 'img/camera.png'
                                });

                                var topoLayer = new L.TopoJSON(null, {
                                    pointToLayer: function(feature, latlng) {
                                        return L.marker(latlng, {
                                            icon: greenIcon
                                        });
                                    },
                                    onEachFeature: function(feature, layer, latlng) {
                                        var modal = '';
                                        $.get("show.php", {
                                            data: '',
                                            height: height_map,
                                            width: width_map
                                        }, function(response) {
                                            modal = response;
                                            var popup = L.popup({
                                                closeButton: true,
                                                minWidth: 900
                                            }).setLatLng([-12.095852707001384, -77.0450011214560]).setContent(modal);
                                            layer.bindPopup(popup);
                                        });
                                    }
                                });

                                $.getJSON('geojson/cam_elalto.topojson', function(topoData) {
                                    topoLayer.addData(topoData);
                                });

                                return topoLayer;
                            }())
                        },
                        {
                            active: false,
                            name: "CCTV MML",
                            layer: (function() {
                                var geojsonMarkerOptions = {
                                    radius: 6,
                                    fillColor: "#ff1010",
                                    color: "#000",
                                    weight: 1,
                                    opacity: 1,
                                    fillOpacity: 0.8
                                };

                                var greenIcon = L.icon({
                                    iconUrl: 'img/camera.png',
                                    shadowUrl: 'img/camera.png'
                                });

                                var topoLayer = new L.TopoJSON(null, {
                                    pointToLayer: function(feature, latlng) {
                                        return L.marker(latlng, {
                                            icon: greenIcon
                                        });
                                    },
                                    onEachFeature: function(feature, layer) {
                                        layer.bindPopup('<p><b>Cámara:</b> ' + feature.properties.name + '</p>')
                                    }
                                });

                                $.getJSON('geojson/camaras_antiguas.topojson', function(topoData) {
                                    topoLayer.addData(topoData);
                                });

                                return topoLayer;
                            }())
                        },
                        {
                            active: false,
                            name: "CCTV Distritos",
                            layer: (function() {
                                var ptz = L.icon({
                                    iconUrl: 'img/DomoPTZ_icon2.png?v=3',
                                    shadowUrl: 'img/DomoPTZ_icon2.png?v=3'
                                });

                                var pez = L.icon({
                                    iconUrl: 'img/CamaraPez_icon2.png?v=3',
                                    shadowUrl: 'img/CamaraPez_icon2.png?v=3'
                                });

                                var fija = L.icon({
                                    iconUrl: 'img/CamaraFija_icon2.png?v=3',
                                    shadowUrl: 'img/CamaraFija_icon2.png?v=3'
                                });

                                var topoLayer = new L.TopoJSON(null, {
                                    pointToLayer: function(feature, latlng) {
                                        switch (feature.properties.tipo_camara_id) {
                                            case 3:
                                                return L.marker(latlng, {
                                                    icon: ptz
                                                });
                                                break;
                                            case 2:
                                                return L.marker(latlng, {
                                                    icon: pez
                                                });
                                                break;
                                            case 1:
                                                return L.marker(latlng, {
                                                    icon: fija
                                                });
                                                break;
                                        }

                                    },
                                    onEachFeature: function(feature, layer) {
                                        //layer.bindPopup('<table border="1" width="100%"><tr><td>Ubigeo</td><td>' + feature.properties.ubigeo + '</td></tr></table>')
                                        var table = '';
                                        table += '<table class="table table-sm table-mariconada" border="0" width="100%">';
                                        table += '<tr>';
                                        table += '<td class="bold"><b>Ubigeo</b></td>';
                                        table += '<td>' + feature.properties.ubigeo + '</td>';
                                        table += '<td class="bold"><b>Municipalidad</b></td>';
                                        table += '<td>' + feature.properties.municipalidad + '</td>';
                                        table += '</tr>';
                                        table += '<tr>';
                                        table += '<td class="bold"><b>Tipo</b></td>';
                                        table += '<td>' + feature.properties.tipo + '</td>';
                                        table += '<td class="bold"><b>Soporte</b></td>';
                                        table += '<td>' + feature.properties.soporte + '</td>';
                                        table += '</tr>';
                                        table += '<tr>';
                                        table += '<td class="bold"><b>Dirección</b></td>';
                                        table += '<td>' + feature.properties.direccion + '</td>';
                                        table += '<td class="bold"><b>Latitud </b></td>';
                                        table += '<td>' + feature.properties.latitud + '</td>';
                                        table += '</tr>';
                                        table += '<tr>';
                                        table += '<td class="bold"><b>Longitud</b></td>';
                                        table += '<td>' + feature.properties.longitud + '</td>';
                                        table += '<td class="bold"><b>Estado </b></td>';
                                        table += '<td>' + feature.properties.estado + '</td>';
                                        table += '</tr>';
                                        table += '<tr>';
                                        table += '<td class="bold"><b>Conectividad</b></td>';
                                        table += '<td>' + feature.properties.conectividad + '</td>';
                                        table += '<td class="bold"><b>Cantidad</b></td>';
                                        table += '<td>' + feature.properties.cantidad + '</td>';
                                        table += '</tr>';
                                        table += '</table>';

                                        layer.bindPopup(table, {
                                            minWidth: 560
                                        });
                                        /*layer.bindPopup(`<b>Ubigeo:</b> ${feature.properties.ubigeo}<br>
                                        <b>Municipalidad:</b> ${feature.properties.municipalidad}<br>
                                        <b>Tipo:</b> ${feature.properties.tipo}<br>
                                        <b>Soporte:</b> ${feature.properties.soporte}<br>
                                        <b>Dirección:</b> ${feature.properties.direccion}<br>
                                        <b>Latitud:</b> ${feature.properties.latitud}<br>
                                        <b>Longitud:</b> ${feature.properties.longitud}<br>
                                        <b>Estado:</b> ${feature.properties.estado}<br>
                                        <b>Conectividad:</b> ${feature.properties.conectividad}<br>
                                        <b>Cantidad:</b> ${feature.properties.cantidad}`*/
                                    }
                                });

                                $.getJSON('geojson/camaras_distritales.topojson?v=5', function(topoData) {
                                    topoLayer.addData(topoData);
                                });

                                return topoLayer;
                            }())
                        },
                        {
                            active: false,
                            name: "Bomberos",
                            layer: (function() {
                                var geojsonMarkerOptions = {
                                    radius: 6,
                                    fillColor: "#ff1010",
                                    color: "#000",
                                    weight: 1,
                                    opacity: 1,
                                    fillOpacity: 0.8
                                };

                                var greenIcon = L.icon({
                                    iconUrl: 'img/bomberos.png',
                                    shadowUrl: 'img/bomberos.png'
                                });

                                var topoLayer = new L.TopoJSON(null, {
                                    pointToLayer: function(feature, latlng) {
                                        return L.marker(latlng, {
                                            icon: greenIcon
                                        });
                                    },
                                    onEachFeature: function(feature, layer) {
                                        layer.bindPopup('<p><b>Estación de bombero:</b> ' + feature.properties.name + '</p>')
                                    }
                                });

                                $.getJSON('geojson/bomberos.topojson', function(topoData) {
                                    topoLayer.addData(topoData);
                                });

                                return topoLayer;
                            }())
                        },
                        {
                            active: false,
                            name: "&nbsp;Comisarías",
                            layer: (function() {
                                var geojsonMarkerOptions = {
                                    radius: 6,
                                    fillColor: "#ff1010",
                                    color: "#000",
                                    weight: 1,
                                    opacity: 1,
                                    fillOpacity: 0.8
                                };

                                var greenIcon = L.icon({
                                    iconUrl: 'img/police.png',
                                    shadowUrl: 'img/police.png'
                                });

                                var topoLayer = new L.TopoJSON(null, {
                                    pointToLayer: function(feature, latlng) {
                                        return L.marker(latlng, {
                                            icon: greenIcon
                                        });
                                    },
                                    onEachFeature: function(feature, layer) {
                                        layer.bindPopup('<p><b>Comisaría:</b> ' + feature.properties.comisaria + '</p>')
                                    }
                                });

                                $.getJSON('geojson/comisarias.topojson', function(topoData) {
                                    topoLayer.addData(topoData);
                                });

                                return topoLayer;
                            }())
                        }
                    ]
                },

                //rutas
                {
                    group: "Rutas ATU ",
                    collapsed: true,
                    layers: [{
                        key: "rutas_lima_callao",
                        active: false,
                        name: "Rutas Lima y Callao",
                        layer: {
                            type: "tileLayer.wms",
                            args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                layers: 'colaboraccion_2020:rutas_lima_callao',
                                format: 'image/png',
                                transparent: true,
                                opacity: 0.6
                            }]
                        }
                    }]

                },
                //rutas aqp
                {
                    group: "Rutas AQP",
                    collapsed: true,
                    layers: [{
                        key: "rutas_aqp",
                        active: false,
                        name: "Rutas AQP",
                        layer: {
                            type: "tileLayer.wms",
                            args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                layers: 'colaboraccion_2020:vias_aqp',
                                format: 'image/png',
                                transparent: true,
                                opacity: 0.6
                            }]
                        }
                    }]

                },
                {
                    group: "Rutas El Alto",
                    collapsed: true,
                    layers: [{
                        key: "rutas_elalto",
                        active: false,
                        name: "Rutas El Alto",
                        layer: {
                            type: "tileLayer.wms",
                            args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                layers: 'colaboraccion_2020:vias_elalto',
                                format: 'image/png',
                                transparent: true,
                                opacity: 0.6
                            }]
                        }
                    }]

                },
                //transito
                {
                    group: "Tránsito",
                    collapsed: true,
                    layers: [
                        {
                            active: false,
                            name: "Semáforos",
                            layer: (function() {
                                var geojsonMarkerOptions = {
                                    radius: 6,
                                    fillColor: "#ff1010",
                                    color: "#000",
                                    weight: 1,
                                    opacity: 1,
                                    fillOpacity: 0.8
                                };

                                var greenIcon = L.icon({
                                    iconUrl: 'img/semaforo.png',
                                    shadowUrl: 'img/semaforo.png'
                                });

                                var topoLayer = new L.TopoJSON(null, {
                                    pointToLayer: function(feature, latlng) {
                                        return L.marker(latlng, {
                                            icon: greenIcon
                                        });
                                    },
                                    onEachFeature: function(feature, layer) {
                                        layer.bindPopup('<p><b>Semaforo:</b> ' + feature.properties.interseccion + '</p>')
                                    }
                                });

                                $.getJSON('geojson/semaforos.topojson', function(topoData) {
                                    topoLayer.addData(topoData);
                                });

                                return topoLayer;
                            }())
                        },
                        {
                            active: false,
                            name: "Semáforos AQP",
                            layer: (function() {
                                var geojsonMarkerOptions = {
                                    radius: 6,
                                    fillColor: "#ff1010",
                                    color: "#000",
                                    weight: 1,
                                    opacity: 1,
                                    fillOpacity: 0.8
                                };

                                var greenIcon = L.icon({
                                    iconUrl: 'img/semaforo.png',
                                    shadowUrl: 'img/semaforo.png'
                                });

                                var topoLayer = new L.TopoJSON(null, {
                                    pointToLayer: function(feature, latlng) {
                                        return L.marker(latlng, {
                                            icon: greenIcon
                                        });
                                    },
                                    onEachFeature: function(feature, layer) {
                                        var table = '';
                                        table += '<table class="table table-sm table-mariconada" border="0" width="100%">';
                                        table += '<tr>';
                                        table += '<td class="bold"><b>Ubigeo</b></td>';
                                        table += '<td>' + feature.properties.departamento + '</td>';
                                        table += '<td class="bold"><b>Tipo de vía</b></td>';
                                        table += '<td>' + feature.properties.tipo_via + '</td>';
                                        table += '</tr>';
                                        table += '<tr>';
                                        table += '<td class="bold"><b>Entrada</b></td>';
                                        table += '<td>' + feature.properties.entrada + '</td>';
                                        table += '<td class="bold"><b>Intersección</b></td>';
                                        table += '<td>' + feature.properties.interseccion + '</td>';
                                        table += '</tr>';
                                        table += '<tr>';
                                        table += '<td class="bold"><b>Latitud</b></td>';
                                        table += '<td>' + feature.properties.latitud + '</td>';
                                        table += '<td class="bold"><b>Longitud </b></td>';
                                        table += '<td>' + feature.properties.longitud + '</td>';
                                        table += '</tr>';
                                        table += '</table>';
                                        layer.bindPopup(table,{minWidth: 560})
                                    }
                                });

                                $.getJSON('geojson/semaforos_aqp.topojson', function(topoData) {
                                    topoLayer.addData(topoData);
                                });

                                return topoLayer;
                            }())
                        },
                        {
                            active: false,
                            name: "Fiscaliza MML",
                            layer: (function() {
                                var geojsonMarkerOptions = {
                                    radius: 6,
                                    fillColor: "#ff1010",
                                    color: "#000",
                                    weight: 1,
                                    opacity: 1,
                                    fillOpacity: 0.8
                                };

                                var greenIcon = L.icon({
                                    iconUrl: 'img/fiscaliza_1.png',
                                    shadowUrl: 'img/fiscaliza_1.png'
                                });

                                var topoLayer = new L.TopoJSON(null, {
                                    pointToLayer: function(feature, latlng) {
                                        return L.marker(latlng, {
                                            icon: greenIcon
                                        });
                                        //return L.circleMarker(latlng, geojsonMarkerOptions);
                                    },
                                    onEachFeature: function(feature, layer) {
                                        layer.bindPopup('<p><b>AVENIDA:</b> ' + feature.properties.avenida + '<br> <b>INTERSECCIÓN:</b> ' + feature.properties.interseccion + '</p>')
                                    }
                                });

                                $.getJSON('geojson/puntos_fiscalizacion.topojson', function(topoData) {
                                    topoLayer.addData(topoData);
                                });

                                return topoLayer;
                            }())
                        },
                        {
                            active: false,
                            name: "Fiscaliza distritos",
                            layer: (function() {
                                var geojsonMarkerOptions = {
                                    radius: 6,
                                    fillColor: "#ff1010",
                                    color: "#000",
                                    weight: 1,
                                    opacity: 1,
                                    fillOpacity: 0.8
                                };

                                var greenIcon = L.icon({
                                    iconUrl: 'img/fiscaliza_1.png',
                                    shadowUrl: 'img/fiscaliza_1.png'
                                });

                                var puntosfisca = [1, 7, 11, 12, 13, 15, 21, 23, 24, 26, 30, 36, 42, 47, 51, 54, 58, 59, 63, 65, 69, 71, 80, 81, 89, 90, 92, 93, 94, 95];

                                var topoLayer = new L.TopoJSON(null, {
                                    filter: function(feature, layer) {
                                        return (puntosfisca.includes(feature.properties.item));
                                    },
                                    pointToLayer: function(feature, latlng) {
                                        return L.marker(latlng, {
                                            icon: greenIcon
                                        });
                                    },
                                    onEachFeature: function(feature, layer) {
                                        layer.bindPopup('<p><b>AVENIDA:</b> ' + feature.properties.avenida + '<br> <b>INTERSECCIÓN:</b> ' + feature.properties.interseccion + '</p>')
                                    }
                                });

                                $.getJSON('geojson/semaforos.topojson', function(topoData) {
                                    topoLayer.addData(topoData);
                                });

                                return topoLayer;
                            }())
                        }
                    ]
                },
                //vias
                {
                    group: "Vías MML",
                    collapsed: true,
                    layers: [{
                            key: "cvnacional",
                            active: false,
                            name: "<i class='glyphicon glyphicon glyphicon-tint vias-nacionales'></i> Nacionales",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:vias_nacionales',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6
                                }]
                            }
                        },
                        {
                            key: "cvmetro",
                            active: false,
                            name: "<i class='glyphicon glyphicon glyphicon-tint vias-metropolitanas'></i> Metropolitanas",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:vias_metropolitanas',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6
                                }]
                            }
                        },
                        {
                            key: "cvarterial",
                            active: false,
                            name: "<i class='glyphicon glyphicon glyphicon-tint vias-arteriales'></i> Vías arteriales",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:vias_arteriales',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6
                                }]
                            }
                        }, {
                            key: "cvcolector",
                            active: false,
                            name: "<i class='glyphicon glyphicon glyphicon-tint vias-colectoras'></i> Vías colectoras",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:vias_colectoras',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6
                                }]
                            }
                        },
                        {
                            active: false,
                            name: "<i class='glyphicon glyphicon glyphicon-tint vias-distritales'></i>  Vías distritales",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:Red_Vial',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6
                                }]
                            }
                        },
                    ]
                },
                //conectividad
                {
                    group: "Conectividad",
                    collapsed: true,
                    layers: [{
                            key: "ccanalizad",
                            active: false,
                            name: "Canalizado ON",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:CANALIZADO_ON',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6
                                }]
                            }
                        },
                        {
                            key: "cfibra",
                            active: false,
                            name: "Fibra ON",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:FIBRA_OPTICAL2020',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6
                                }]
                            }
                        },
                        {
                            active: false,
                            name: "Fibra Surco",
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:distrito_surco',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6
                                }]
                            }
                        }
                    ]
                },
                //hexagonos
                {
                    key: "ghexagonos",
                    group: "mask_hexagonos",
                    layers: [{
                            key: "hexagonos",
                            name: "&nbsp;Todos",
                            table: 'tmp_20201020_1',
                            anio: '1=1',
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:hex_lima_callao',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6
                                }]
                            }
                        },
                        {
                            key: "hexagonos2016",
                            name: "&nbsp;2016",
                            table: 'delitos_2016',
                            anio: 'anio=2016',
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:hex_lima_callao_2016',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6
                                }]
                            }
                        },
                        {
                            key: "hexagonos2017",
                            name: "&nbsp;2017",
                            table: 'delitos_2017',
                            anio: 'anio=2017',
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:hex_lima_callao_2017',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6
                                }]
                            }
                        },
                        {
                            key: "hexagonos2018",
                            name: "&nbsp;2018",
                            table: 'delitos_2018',
                            anio: 'anio=2018',
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:hex_lima_callao_2018',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6
                                }]
                            }
                        },
                        {
                            key: "hexagonos2019",
                            name: "&nbsp;2019",
                            table: 'delitos_2019',
                            anio: 'anio=2019',
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:hex_lima_callao_2019',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6
                                }]
                            }
                        },
                        {
                            key: "hexagonos2020",
                            name: "&nbsp;2020",
                            table: 'delitos_2017',
                            anio: 'anio=2017',
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:hex_lima_callao_2017',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6
                                }]
                            }
                        },
                        {
                            key: "hexagonos2021",
                            name: "&nbsp;2021",
                            table: 'delitos_2016',
                            anio: 'anio=2016',
                            layer: {
                                type: "tileLayer.wms",
                                args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
                                    layers: 'colaboraccion_2020:hex_lima_callao_2016',
                                    format: 'image/png',
                                    transparent: true,
                                    opacity: 0.6
                                }]
                            }
                        },

                    ]
                }
            ];

            var panelLayer = new L.Control.PanelLayers(baseLayers, overLayers, {
                title: 'Lima Segura',
                position: 'topright',
                compact: true,
                collapsibleGroups: true
            });

            map.addControl(panelLayer);

            panelLayer.on('panel:selected', function(feature) {
                console.log("panel", feature)
                if ('key' in feature) {
                    const f = feature.key;
                    switch (feature.key) {
                        case 'hex_pnp':
                            if (document.querySelector(`.LegendHexPreventivos`) != null) {
                                removeLegendFromMap('LegendHexPreventivos');
                                removeLegendFromMap('info');
                            }
                            if (document.querySelector(`.LegendPointNoPreventivos`) != null) {
                                removeLegendFromMap('LegendPointNoPreventivos');
                                removeLegendFromMap('info');
                            }
                            if (document.querySelector(`.LegendPointPreventivos`) != null) {
                                removeLegendFromMap('LegendPointPreventivos');
                                removeLegendFromMap('info');
                            }
                            // removeLegendFromMap('LegendPointPreventivos');
                            addLegendToMap('LegendHexNoPreventivos', 'Hexagonos No Preventivos (PNP+Sereno)', '_NP');
                            removeLegendFromMap('info');
                            break;
                        case 'hex_no_preventivos':
                            if (document.querySelector(`.LegendHexPreventivos`) != null) {
                                removeLegendFromMap('LegendHexPreventivos');
                                removeLegendFromMap('info');
                            }
                            if (document.querySelector(`.LegendPointNoPreventivos`) != null) {
                                removeLegendFromMap('LegendPointNoPreventivos');
                                removeLegendFromMap('info');
                            }
                            if (document.querySelector(`.LegendPointPreventivos`) != null) {
                                removeLegendFromMap('LegendPointPreventivos');
                                removeLegendFromMap('info');
                            }
                            // removeLegendFromMap('LegendPointPreventivos');
                            addLegendToMap('LegendHexNoPreventivos', 'Hexagonos No Preventivos (PNP+Sereno)', '_NP');
                            removeLegendFromMap('info');
                            break;
                        case 'hex_preventivos':
                            if (document.querySelector(`.LegendHexNoPreventivos`) != null) {
                                removeLegendFromMap('LegendHexNoPreventivos');
                                removeLegendFromMap('info');
                            }
                            if (document.querySelector(`.LegendPointNoPreventivos`) != null) {
                                removeLegendFromMap('LegendPointNoPreventivos');
                                removeLegendFromMap('info');
                            }
                            if (document.querySelector(`.LegendPointPreventivos`) != null) {
                                removeLegendFromMap('LegendPointPreventivos');
                                removeLegendFromMap('info');
                            }
                            addLegendToMap('LegendHexPreventivos', 'Hexagonos Preventivos (Sereno)', '_P');
                            removeLegendFromMap('info');
                            break;
                        case 'nopreventivos':
                            if (document.querySelector(`.LegendHexNoPreventivos`) != null) {
                                removeLegendFromMap('LegendHexNoPreventivos');
                                removeLegendFromMap('info');
                            }
                            if (document.querySelector(`.LegendHexPreventivos`) != null) {
                                removeLegendFromMap('LegendHexPreventivos');
                                removeLegendFromMap('info');
                            }
                            if (document.querySelector(`.LegendPointPreventivos`) != null) {
                                removeLegendFromMap('LegendPointPreventivos');
                                removeLegendFromMap('info');
                            }

                            addLegendToMap('LegendPointNoPreventivos', 'Puntos No Preventivos (PNP+Sereno)', 'P_NP');
                            removeLegendFromMap('info');
                            break;
                        case 'preventivos':
                            if (document.querySelector(`.LegendHexNoPreventivos`) != null) {
                                removeLegendFromMap('LegendHexNoPreventivos');
                                removeLegendFromMap('info');
                            }
                            if (document.querySelector(`.LegendHexPreventivos`) != null) {
                                removeLegendFromMap('LegendHexPreventivos');
                                removeLegendFromMap('info');
                            }
                            if (document.querySelector(`.LegendPointNoPreventivos`) != null) {
                                removeLegendFromMap('LegendPointNoPreventivos');
                                removeLegendFromMap('info');
                            }
                            addLegendToMap('LegendPointPreventivos', 'Puntos Preventivos (Sereno)', 'P_P');
                            removeLegendFromMap('info');
                            break;
                        case 'delitosAlto':
                            map.setView(new L.LatLng(-4.267261925325336, -81.21831884445973), 16);
                            $('#map').css('cursor', 'pointer');
                            break;
                        case 'delitosLal':
                            map.setView(new L.LatLng(-8.129351929999928, -79.03269670999998), 15);

                            $('#map').css('cursor', 'pointer');
                            break;
                        case 'delitosaqp':
                            map.setView(new L.LatLng(-16.40047044987634, -71.53567796328004), 14);
                            $('#map').css('cursor', 'pointer');
                            break;
                        case 'densidadaqp':
                            map.setView(new L.LatLng(-16.40047044987634, -71.53567796328004), 14);
                            $('#map').css('cursor', 'pointer');
                            break;
                    }

                }

                if ('key' in feature && !excludeLayers.includes(feature.key)) {
                    cql_or.push(feature.key + ' > 0');
                    console.log("aquiva1")
                    hexagonos_filter();
                }

                if (feature.key == 'rutas_aqp') {
                    // console.log(feature.key)
                    hexagonos_filter2(feature.key);
                }
                if (feature.key == 'rutas_elalto') {
                    // console.log(feature.key)
                    hexagonos_filter2(feature.key);
                }
                if (feature.key == 'ubigeos') {
                    // console.log(feature.key)
                    hexagonos_filter2(feature.key);
                }


                if ('key' in feature && excludeLayers.slice(0, 6).includes(feature.key)) {
                    var lyr = overLayers.filter(p => p.group == 'mask_hexagonos').shift().layers;
                    console.log("aquiva1")

                    if ($('#chkHexagonos:checked').length > 0) {
                        $('#map').css('cursor', 'pointer');
                        console.log("ddddd");
                    }
                    if ($('#chkHexagonos:checked').length == 0) {
                        for (i = 0; i < lyr.length; i++) {
                            if (map.hasLayer(lyr[i].layer)) {
                                map.removeLayer(lyr[i].layer);
                            }
                        }
                    }
                }

                if ('key' in feature && ['mask_hexagonos', 'mask_hexagonos2016', 'mask_hexagonos2017', 'mask_hexagonos2018', 'mask_hexagonos2019', 'mask_hexagonos2020', 'mask_hexagonos2021'].includes(feature.key)) {
                    puntos_filter();
                    console.log("aquiva1")

                    if ($('#chkHexagonos').is(':checked')) {
                        var lyr = overLayers.filter(p => p.group == 'mask_hexagonos').shift().layers;

                        for (i = 0; i < lyr.length; i++) {
                            if (map.hasLayer(lyr[i].layer)) {
                                map.removeLayer(lyr[i].layer);
                            }
                        }
                        console.log("aquiva1")

                        map.addLayer(lyr.filter(p => p.key == feature.parent).shift().layer);
                    }

                    if (lyr_hex_idx == 0) {
                        lyr_hex_idx++;
                        return;
                    }

                    if (lyr_mapa_calor_idx.length > 0) {
                        var layerid = $('.leaflet-panel-layers-group input[type="radio"]:checked');
                        var idx = lyr_mapa_calor_idx;
                        lyr_mapa_calor_idx = [];

                        lyr_mapa_calor.clearLayers();

                        for (i = 0; i < idx.length; i++) {
                            lyr_mapa_calor_idx.push(idx[i]);
                            mapa_calor_distrito(idx[i], layerid.val(), null);
                        }
                    }
                }

                if ('key' in feature && ['dgenericos', 'faltas', 'modalidad'].includes(feature.key)) {
                    cql_puntos.push(feature.cql);
                    puntos_filter();
                    console.log("aquiva1")

                }
            });

            panelLayer.on('panel:unselected', function(feature) {

                if ('key' in feature && !excludeLayers.includes(feature.key)) {
                    var index = cql_or.indexOf(feature.key + ' > 0');
                    cql_or.splice(index, 1);

                    hexagonos_filter();
                }
                if (feature.key == 'rutas_aqp') {
                    // console.log(feature.key)
                    hexagonos_filter2(feature.key, 'unselected');
                }

                if (feature.key == 'rutas_elalto') {
                    // console.log(feature.key)
                    hexagonos_filter2(feature.key, 'unselected');
                }

                if (feature.key == 'ubigeos') {
                    // console.log(feature.key)
                    hexagonos_filter2(feature.key, 'unselected');
                }

                if ('key' in feature && excludeLayers.slice(0, 6).includes(feature.key)) {
                    $('#map').css('cursor', 'grab');
                }

                if ('key' in feature && ['dgenericos', 'faltas', 'modalidad'].includes(feature.key)) {
                    var index = cql_or.indexOf(feature.cql);
                    cql_puntos.splice(index, 1);

                    puntos_filter();

                    /*var anio = overLayers.filter(p => p.group == 'mask_hexagonos').shift().layers.filter(p => p.key == feature.parent).shift().anio;
                    var puntoslyr = overLayers.filter(p => p.key == 'gpuntos').shift().layers.filter(p => p.key == 'puntos').shift().layer;
                    puntoslyr.wmsParams.cql_filter = anio;
                    puntoslyr.redraw();*/

                    /*var puntoslyr = overLayers.filter(p => p.key == 'gpuntos').shift().layers.filter(p => p.key == 'puntos').shift().layer;
                    var index = cql_or.indexOf(feature.cql);
                    cql_puntos.splice(index, 1);*/

                    //console.log(cql_puntos.length == 0 ? '1=1 and ' + anio : anio + ' and ' + cql_puntos.join(' or '));
                }
            });

            var str = '<div class="leaflet-panel-layers-item" style=""><label class="leaflet-panel-layers-title"><input id="chkHexagonos" class="leaflet-panel-layers-selector" type="checkbox" style="float: left; margin-top: 2px;"><span class="">&nbsp;<img class="delitoPNP" src="./assets/img/hexRojo.jpg" style="width:12px;heigth:12px">&nbsp;Comisarias PNP</span></label></div>';
            $(str).insertBefore($('.leaflet-panel-layers-list'));
        }

        let addLegendToMap = (legendClassName = '', title = '', tipo = '') => {
            if (document.querySelector(`.${legendClassName}`) != null) document.querySelector(`.${legendClassName}`).remove();

            var legend = L.control({
                position: "bottomleft"
            });

            legend.onAdd = function(map) {
                var div = L.DomUtil.create("div", legendClassName);

                div.innerHTML = setLegend(title, tipo);

                return div;
            };

            legend.addTo(map);
        }

        let setLegend = (title = '', tipo = '') => {
            let html = '';
            let colorRojo = ["8C0000", "D90000", "FF5C26"];
            let colorVerde = ["017d3a", "7FD7A7", "BFEBD2"];
            let colorAmarillo = ["fd0012", "45a012", "fffe30"];

            // let labels = ["Muy Alto", "Alto", "Medio", "Bajo", "Muy Bajo"];
            let labels = ["Alto", "Medio", "Bajo"];
            let labelsPuntos = ["No preventivos - PNP", "No preventivos - Sereno", "Preventivos Sereno"];

            html += `<h4>${title}</h4>`;
            if (tipo == '_NP') {
                html += labels.map((label, i) => `<i style="background: #${colorRojo[i]}"></i><span>${labels[i]}</span><br>`).join('');
            }
            if (tipo == '_P') {
                html += labels.map((label, i) => `<i style="background: #${colorVerde[i]}"></i><span>${labels[i]}</span><br>`).join('');
            }

            if (tipo == 'P_NP' || tipo == 'P_P') {
                html += labels.map((label, i) => `<i style="background: #${colorAmarillo[i]}"></i><span>${labelsPuntos[i]}</span><br>`).join('');
            }

            // html += `<h4 style="text-align: left">${subtitle}</h4>`;
            return html;
        }

        let removeLegendFromMap = (legendClassName = '') => {
            document.querySelector(`.${legendClassName}`).remove();
        }


        function puntos_filter() {
            var layerid = $('.leaflet-panel-layers-group input[type="radio"]:checked');
            var layer = overLayers.filter(p => p.key == 'gpuntos').shift().layers.filter(p => p.key == 'puntos').shift().layer;

            if (layerid.length > 0 && map.hasLayer(layer)) {
                var parent = baseLayers.filter(p => p.key == 'ghexagonos').shift().layers.filter(p => p.id == layerid.val()).shift().parent;
                var anio = overLayers.filter(p => p.group == 'mask_hexagonos').shift().layers.filter(p => p.key == parent).shift().anio;
                var puntoslyr = overLayers.filter(p => p.key == 'gpuntos').shift().layers.filter(p => p.key == 'puntos').shift().layer;

                if (!map.hasLayer(puntoslyr)) {
                    map.addLayer(puntoslyr);
                }

                console.log(anio + ' and ' + (cql_puntos.length == 0 ? '1=1' : cql_puntos.join(' or ')));
                puntoslyr.wmsParams.cql_filter = anio + ' and ' + (cql_puntos.length == 0 ? '1=1' : cql_puntos.join(' or '));
                puntoslyr.redraw();

            }
        }

        function hexagonos_filter2(key, unset = '') {
            _cql = [];

            if (unset != 'unselected' && key!='ubigeos') {
                _cql.push('rutas > 0');
            }

            if (unset != 'unselected' && key=='ubigeos') {
                _cql.push('cubigeo > 0');
            }

            filtrar_imagen2(key, (unset != 'unselected' ? _cql.join(' and ') : '1=1'));
        }

        function hexagonos_filter() {
            _cql = [];

            if (cql_and.length > 0) {
                _cql.push(cql_and.join(' and '));
            }

            if (cql_or.length > 0) {
                _cql.push('(' + cql_or.join(' or ') + ')');
            }

            filtrar_imagen((_cql.length == 0 ? '1=1' : _cql.join(' and ')));
        }

        function filtrar_imagen(cql) {
            var layerid = $('.leaflet-panel-layers-group input[type="radio"]:checked');

            if (layerid.length > 0) {
                var parent = baseLayers.filter(p => p.key == 'ghexagonos').shift().layers.filter(p => p.id == layerid.val()).shift().parent;
                var baselayer = overLayers.filter(p => p.group == 'mask_hexagonos').shift().layers.filter(p => p.key == parent).shift().layer;

                baselayer.wmsParams.cql_filter = cql;
                baselayer.redraw();
            }
        }

        function filtrar_imagen2(key, cql) {
            if (map.hasLayer(overLayers.filter(p => p.key == 'delitosaqp').shift().layer)) {
                var overlayer = overLayers.filter(p => p.key == 'delitosaqp').shift().layer;
                overlayer.wmsParams.cql_filter = cql;
                overlayer.redraw();
            }

            if (map.hasLayer(overLayers.filter(p => p.key == 'delitosAlto').shift().layer)) {
                var overlayer = overLayers.filter(p => p.key == 'delitosAlto').shift().layer;
                overlayer.wmsParams.cql_filter = cql;
                overlayer.redraw();
            }

            if (map.hasLayer(overLayers.filter(p => p.key == 'densidadaqp').shift().layer)) {
                var overlayer = overLayers.filter(p => p.key == 'densidadaqp').shift().layer;
                overlayer.wmsParams.cql_filter = cql;
                overlayer.redraw();
            }

            if (map.hasLayer(overLayers.filter(a=>a.key=='ghexagonos').shift().layers.filter(a=>a.key=='hexagonos').shift().layer)) {
                var overlayer = overLayers.filter(a=>a.key=='ghexagonos').shift().layers.filter(a=>a.key=='hexagonos').shift().layer;
                overlayer.wmsParams.cql_filter = cql;
                overlayer.redraw();
            }
            if (map.hasLayer(overLayers.filter(a=>a.key=='ghexagonos').shift().layers.filter(a=>a.key=='hexagonos2016').shift().layer)) {
                var overlayer = overLayers.filter(a=>a.key=='ghexagonos').shift().layers.filter(a=>a.key=='hexagonos2016').shift().layer;
                overlayer.wmsParams.cql_filter = cql;
                overlayer.redraw();
            }
            if (map.hasLayer(overLayers.filter(a=>a.key=='ghexagonos').shift().layers.filter(a=>a.key=='hexagonos2017').shift().layer)) {
                var overlayer = overLayers.filter(a=>a.key=='ghexagonos').shift().layers.filter(a=>a.key=='hexagonos2017').shift().layer;
                overlayer.wmsParams.cql_filter = cql;
                overlayer.redraw();
            }
            if (map.hasLayer(overLayers.filter(a=>a.key=='ghexagonos').shift().layers.filter(a=>a.key=='hexagonos2018').shift().layer)) {
                var overlayer = overLayers.filter(a=>a.key=='ghexagonos').shift().layers.filter(a=>a.key=='hexagonos2018').shift().layer;
                overlayer.wmsParams.cql_filter = cql;
                overlayer.redraw();
            }
            if (map.hasLayer(overLayers.filter(a=>a.key=='ghexagonos').shift().layers.filter(a=>a.key=='hexagonos2019').shift().layer)) {
                var overlayer = overLayers.filter(a=>a.key=='ghexagonos').shift().layers.filter(a=>a.key=='hexagonos2019').shift().layer;
                overlayer.wmsParams.cql_filter = cql;
                overlayer.redraw();
            }
            if (map.hasLayer(overLayers.filter(a=>a.key=='ghexagonos').shift().layers.filter(a=>a.key=='hexagonos2020').shift().layer)) {
                var overlayer = overLayers.filter(a=>a.key=='ghexagonos').shift().layers.filter(a=>a.key=='hexagonos2020').shift().layer;
                overlayer.wmsParams.cql_filter = cql;
                overlayer.redraw();
            }
            if (map.hasLayer(overLayers.filter(a=>a.key=='ghexagonos').shift().layers.filter(a=>a.key=='hexagonos2021').shift().layer)) {
                var overlayer = overLayers.filter(a=>a.key=='ghexagonos').shift().layers.filter(a=>a.key=='hexagonos2021').shift().layer;
                overlayer.wmsParams.cql_filter = cql;
                overlayer.redraw();
            }

        }


        function actualizar_mapa() {
            var ubigeos = $("#ddlUbigeo option:selected").map(function() {
                return "'" + $(this).val() + "'";
            }).get().join(', ');

            var index = cql_and.findIndex(a => a.includes("iddist"));

            if (index != -1) {
                cql_and.splice(index, 1);
            }

            cql_and.push('iddist in (' + ubigeos + ')');

            hexagonos_filter(ubigeos);
            redraw_layer('ubigeos', 'IDDIST in (' + ubigeos + ')');
            refresh_charts(ubigeos);
        }

        function redraw_layer(layer, cql) {
            overLayers.filter(p => p.key == layer).shift().layer.wmsParams.cql_filter = cql;
            overLayers.filter(p => p.key == layer).shift().layer.redraw();
        }

        function refresh_charts(ubigeos) {
            fs_grafico({
                method: 'hex_x_categoria',
                grafico: 'hex_x_categoria',
                ubigeos: ubigeos
            });
            fs_grafico({
                method: 'vias_nacionales',
                grafico: 'vias_nacionales',
                ubigeos: ubigeos
            });
            fs_grafico({
                method: 'vias_metropolitanas',
                grafico: 'vias_metropolitanas',
                ubigeos: ubigeos
            });
            fs_grafico({
                method: 'vias_arteriales',
                grafico: 'vias_arteriales',
                ubigeos: ubigeos
            });
            fs_grafico({
                method: 'vias_colectoras',
                grafico: 'vias_colectoras',
                ubigeos: ubigeos
            });
        }

        function fs_grafico(params) {
            $.getJSON('php/app.php?data=' + encodeURIComponent(JSON.stringify(params)), function(response) {
                var response = JSON.parse(response);
                // console.log(response);
                if ($('#' + params.grafico).length > 0) {
                    Highcharts.chart(params.grafico, response);
                }

            });
        }
    </script>
</body>

</html>