<?php
require_once 'php/app.php';
$data = json_decode($_GET['data']);
$fs_vias_dist = call_function((object) ['method' => 'fs_vias_dist', 'codvia' => str_replace($data->codvia, "ccanalizad", "ccanalizado") . '>0']);

foreach ($fs_vias_dist as $key_via) {
    ?>
    <tr>
        <td><a href="#" data-cql="iddist='<?php echo $key_via->codubigeo; ?> and <?php echo $data->codvia . '>0'; ?>'"><?php echo $key_via->ubigeo; ?></a></td>
        <td class="text-right"><a href="#" data-cql="iddist='<?php echo $key_via->codubigeo; ?>' and clasifica = 5 and <?php echo $data->codvia . '>0'; ?>"><?php echo $key_via->muy_alto; ?></a></td>
        <td class="text-right"><a href="#" data-cql="iddist='<?php echo $key_via->codubigeo; ?>' and clasifica = 4 and <?php echo $data->codvia . '>0'; ?>"><?php echo $key_via->alto; ?></a></td>
        <td class="text-right"><a href="#" data-cql="iddist='<?php echo $key_via->codubigeo; ?>' and clasifica = 3 and <?php echo $data->codvia . '>0'; ?>"><?php echo $key_via->medio; ?></a></td>
        <td class="text-right"><a href="#" data-cql="iddist='<?php echo $key_via->codubigeo; ?>' and clasifica = 2 and <?php echo $data->codvia . '>0'; ?>"><?php echo $key_via->bajo; ?></a></td>
        <td class="text-right"><a href="#" data-cql="iddist='<?php echo $key_via->codubigeo; ?>' and clasifica = 1 and <?php echo $data->codvia . '>0'; ?>"><?php echo $key_via->muy_bajo; ?></a></td>
        <td class="text-right"><a href="#" data-cql="iddist='<?php echo $key_via->codubigeo; ?> and <?php echo $data->codvia . '>0'; ?>'"><?php echo $key_via->total; ?></a></td>
    </tr>
<?php }?>