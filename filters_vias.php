<?php
require_once 'php/app.php';
$data = json_decode($_GET['data']);
$fs_hexagonos_vias = call_function((object) ['method' => 'fs_hexagonos_vias', 'codubigeo' => $data->ubigeo]);

foreach ($fs_hexagonos_vias as $key_via) {
    ?>
    <tr>
        <td><a href="#" data-cql="iddist='<?php echo $data->ubigeo; ?>' and <?php echo $key_via->cod_via; ?> = 1"><?php echo $key_via->via; ?></a></td>
        <td class="text-right"><a href="#" data-ubigeo="IDDIST='<?php echo $data->ubigeo; ?>'" data-cql="iddist='<?php echo $data->ubigeo; ?>' and clasifica = 5 and <?php echo $key_via->cod_via; ?> = 1"><?php echo $key_via->muy_alto; ?></a></td>
        <td class="text-right"><a href="#" data-ubigeo="IDDIST='<?php echo $data->ubigeo; ?>'" data-cql="iddist='<?php echo $data->ubigeo; ?>' and clasifica = 4 and <?php echo $key_via->cod_via; ?> = 1"><?php echo $key_via->alto; ?></a></td>
        <td class="text-right"><a href="#" data-ubigeo="IDDIST='<?php echo $data->ubigeo; ?>'" data-cql="iddist='<?php echo $data->ubigeo; ?>' and clasifica = 3 and <?php echo $key_via->cod_via; ?> = 1"><?php echo $key_via->medio; ?></a></td>
        <td class="text-right"><a href="#" data-ubigeo="IDDIST='<?php echo $data->ubigeo; ?>'" data-cql="iddist='<?php echo $data->ubigeo; ?>' and clasifica = 2 and <?php echo $key_via->cod_via; ?> = 1"><?php echo $key_via->bajo; ?></a></td>
        <td class="text-right"><a href="#" data-ubigeo="IDDIST='<?php echo $data->ubigeo; ?>'" data-cql="iddist='<?php echo $data->ubigeo; ?>' and clasifica = 1 and <?php echo $key_via->cod_via; ?> = 1"><?php echo $key_via->muy_bajo; ?></a></td>
        <td class="text-right"><a href="#" data-ubigeo="IDDIST='<?php echo $data->ubigeo; ?>'" data-cql="iddist='<?php echo $data->ubigeo; ?>' and <?php echo $key_via->cod_via; ?> = 1"><?php echo $key_via->total; ?></a></td>
    </tr>
<?php }?>