<div id="sidebar2" class="bcg-transparent">
	<div class="panel panel-default not-edge not-background">
		<div class="panel-heading text-left">
			<strong>GRÁFICOS ESTADÍSTICOS</strong>
		</div>
		<div class="panel-body">
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default not-edge">
					<div class="panel-heading" role="tab">
						<h4 class="panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#v_hex_x_categoria">
								Total de casos por categoría
							</a>
						</h4>
					</div>
					<div id="v_hex_x_categoria" class="panel-collapse collapse in" role="tabpanel">
						<div class="panel-body">
							<div id="hex_x_categoria" style="height: 350px"></div>
						</div>
					</div>
				</div>
				<div class="panel panel-default not-edge">
					<div class="panel-heading" role="tab">
						<h4 class="panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#v_vias_nacionales">
								Total de casos por vías nacionales
							</a>
						</h4>
					</div>
					<div id="v_vias_nacionales" class="panel-collapse collapse in" role="tabpanel">
						<div class="panel-body">
							<div id="vias_nacionales" style="height: 350px"></div>
						</div>
					</div>
				</div>
				<div class="panel panel-default not-edge">
					<div class="panel-heading" role="tab">
						<h4 class="panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#v_vias_metropolitanas">
								Total de casos por vías metropolitanas
							</a>
						</h4>
					</div>
					<div id="v_vias_metropolitanas" class="panel-collapse collapse in" role="tabpanel">
						<div class="panel-body">
							<div id="vias_metropolitanas" style="height: 350px"></div>
						</div>
					</div>
				</div>
				<div class="panel panel-default not-edge">
					<div class="panel-heading" role="tab">
						<h4 class="panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#vias_arteriales">
							Total de casos por vías arteriales
							</a>
						</h4>
					</div>
					<div id="v_vias_arteriales" class="panel-collapse collapse in" role="tabpanel">
						<div class="panel-body">
							<div id="vias_arteriales" style="height: 350px"></div>
						</div>
					</div>
				</div>
				<div class="panel panel-default not-edge">
					<div class="panel-heading" role="tab">
						<h4 class="panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#vias_colectoras">
								Total de casos por vías colectoras
							</a>
						</h4>
					</div>
					<div id="v_vias_colectoras" class="panel-collapse collapse in" role="tabpanel">
						<div class="panel-body">
							<div id="vias_colectoras" style="height: 350px"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
